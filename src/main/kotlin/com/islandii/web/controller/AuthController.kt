package com.islandii.web.controller

import com.islandii.web.model.dto.Credentials
import com.islandii.web.model.entity.User
import com.islandii.web.service.UserService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid


@RestController
@RequestMapping
class AuthController(private val userService: UserService) {

    @PostMapping("login")
    fun login(@RequestBody credentials: Credentials) =
        ResponseEntity.ok().build<Unit>()

    @PostMapping("signup")
    fun signup(@RequestBody @Valid user: User) =
        ResponseEntity.ok().body(userService.createUser(user))
}