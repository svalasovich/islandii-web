package com.islandii.web.controller

import com.islandii.web.model.dto.HotelSearch
import com.islandii.web.model.projection.HotelSearchProjection
import com.islandii.web.service.HotelService
import org.springframework.data.domain.Pageable
import org.springframework.data.projection.ProjectionFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("\${spring.data.rest.base-path}/hotels")
class HotelController(
    private val hotelService: HotelService,
    private val projectionFactory: ProjectionFactory
) {

    @GetMapping("search")
    fun searchHotel(@Valid hotelSearch: HotelSearch, pageable: Pageable) =
        hotelService.searchHotel(hotelSearch, pageable)
            .let { results ->
                results.map {
                    projectionFactory.createProjection(HotelSearchProjection::class.java, it)
                }
            }
}