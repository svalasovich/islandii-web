package com.islandii.web.component.converter.parameter

import com.islandii.web.model.value.LocalizedId
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import java.util.*

@Component
class LocalizedIdConverter : Converter<String, LocalizedId> {

    private companion object {
        const val SPLITTER = "+"
    }

    private val availableLocales = Locale.getAvailableLocales()
        .asSequence()
        .filter { it.country.isNotBlank() }
        .map { it.toLanguageTag() to it }
        .toMap()

    override fun convert(source: String) =
        source.split(SPLITTER).let { values ->
            LocalizedId(availableLocales[values[1]] ?: throw IllegalArgumentException(values[1]), values[0].toLong())
        }


}