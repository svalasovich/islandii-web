package com.islandii.web.component.converter.parameter

import com.islandii.web.model.dto.HotelRequirements
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class HotelRequirementsConverter : Converter<String, HotelRequirements> {

    private companion object {
        const val SPLITTER = "-"
    }

    override fun convert(source: String) =
        source.split(SPLITTER)
            .let { values -> HotelRequirements(values[0].toInt(), values[1].toInt(), values[2].toInt()) }
}