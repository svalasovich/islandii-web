package com.islandii.web.component.converter.json

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import org.springframework.boot.jackson.JsonComponent
import java.util.*

class LocaleJsonDeserializer : JsonDeserializer<Locale>() {

    private val availableLocales = Locale.getAvailableLocales()
        .asSequence()
        .filter { it.country.isNotBlank() }
        .map { it.toString() to it }
        .toMap()

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Locale {
        if (p.currentToken == JsonToken.VALUE_STRING) {
            return availableLocales.getValue(p.valueAsString)
        }
        throw IllegalArgumentException("Value ${p.currentName} should be string.")
    }
}