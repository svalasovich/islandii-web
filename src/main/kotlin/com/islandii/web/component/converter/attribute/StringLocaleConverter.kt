package com.islandii.web.component.converter.attribute

import java.util.*
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply = true)
class StringLocaleConverter : AttributeConverter<Locale, String> {

    private val availableLocales = Locale.getAvailableLocales()
        .asSequence()
        .filter { it.country.isNotBlank() }
        .map { it.toLanguageTag() to it }
        .toMap()

    override fun convertToDatabaseColumn(attribute: Locale): String = attribute.toLanguageTag()

    override fun convertToEntityAttribute(dbData: String): Locale = availableLocales.getValue(dbData)
}