package com.islandii.web.component.converter.attribute

import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply = true)
class StringSetConverter : AttributeConverter<Set<String>, String> {

    private companion object {
        const val SPLITTER = ";"
    }

    override fun convertToDatabaseColumn(attribute: Set<String>): String {
        return attribute.joinToString(separator = SPLITTER)
    }

    override fun convertToEntityAttribute(dbData: String): Set<String> {
        return dbData.takeIf { it.isNotBlank() }?.split(SPLITTER)?.toSet() ?: emptySet()
    }
}