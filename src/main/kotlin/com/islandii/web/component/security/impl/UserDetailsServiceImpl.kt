package com.islandii.web.component.security.impl

import com.islandii.web.model.entity.User
import com.islandii.web.repository.UserRepository
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsPasswordService
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder

class UserDetailsServiceImpl(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder
) : UserDetailsService, UserDetailsPasswordService {

    override fun loadUserByUsername(username: String): UserDetails {
        return userRepository.findByUsername(username)
            ?.let { UserDetailsImpl(it) }
            ?: throw UsernameNotFoundException("User with name $username not found")
    }

    override fun updatePassword(user: UserDetails, newPassword: String) =
        userRepository.findByUsername(user.username)
            ?.let { User(it.username, it.email, passwordEncoder.encode(newPassword), id = it.id) }
            ?.let { userRepository.save(it) }
            ?.let { UserDetailsImpl(it) }
            ?: throw UsernameNotFoundException("User with name ${user.username} not found")

    class UserDetailsImpl(private val user: User) : UserDetails {

        override fun getAuthorities() = emptyList<GrantedAuthority>()

        override fun getPassword() = user.password

        override fun getUsername() = user.username

        override fun isAccountNonExpired() = true

        override fun isAccountNonLocked() = true

        override fun isCredentialsNonExpired() = true

        override fun isEnabled() = true
    }
}
