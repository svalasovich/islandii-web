package com.islandii.web.component.security.impl

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.islandii.web.component.security.JWTProvider
import com.islandii.web.model.dto.Credentials
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTAuthenticationFilter(
    authenticationManager: AuthenticationManager,
    private val objectMapper: ObjectMapper,
    private val jwtProvider: JWTProvider
) : UsernamePasswordAuthenticationFilter() {

    companion object {
        const val TOKEN_TYPE = "Bearer"
    }

    init {
        setAuthenticationManager(authenticationManager)
    }

    override fun attemptAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse
    ): Authentication {
        if (request.method != "POST") {
            throw AuthenticationServiceException("Authentication method not supported: " + request.method)
        }
        val credentials = objectMapper.readValue(request.inputStream, Credentials::class.java)
        return UsernamePasswordAuthenticationToken(
            credentials.username.trim(),
            credentials.password
        ).let { authenticationManager.authenticate(it) }
    }

    override fun successfulAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse,
        chain: FilterChain,
        authResult: Authentication
    ) {
        val token = (authResult.principal as UserDetails).let {
            Token(jwtProvider.generateToken(it.username), TOKEN_TYPE, jwtProvider.expiration)
        }

        response.contentType = APPLICATION_JSON_VALUE
        objectMapper.writeValue(response.writer, token)
    }

    private data class Token(

        @JsonProperty("access_token")
        val token: String,

        @JsonProperty("token_type")
        val type: String,

        @JsonProperty("expires_in")
        val expiration: Long
    )
}
