package com.islandii.web.component.security

interface JWTProvider {

    val expiration: Long

    fun generateToken(username: String): String

    fun validateToken(token: String): Boolean

    fun getUsernameFromToken(token: String): String
}