package com.islandii.web.component.security.impl

import com.islandii.web.component.security.JWTProvider
import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*


@Component
class JWTProviderImpl(
    @Value("\${jwt.secret}") private val jwtSecret: String,
    @Value("\${jwt.expiration}") override val expiration: Long
) : JWTProvider {

    private val logger = LoggerFactory.getLogger(JWTProviderImpl::class.java)

    override fun generateToken(username: String): String =
        Jwts.builder()
            .setSubject(username)
            .setExpiration(Date.from(Instant.now().plusSeconds(expiration)))
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact()

    override fun validateToken(token: String): Boolean {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token)
            return true
        } catch (expEx: ExpiredJwtException) {
            logger.warn("Token: $token expired")
        } catch (unsEx: UnsupportedJwtException) {
            logger.warn("Unsupported jwt for token: $token")
        } catch (mjEx: MalformedJwtException) {
            logger.warn("Malformed jwt for token: $token")
        } catch (sEx: SignatureException) {
            logger.warn("Invalid signature for token: $token")
        } catch (e: Exception) {
            logger.warn("invalid token $token")
        }
        return false
    }

    override fun getUsernameFromToken(token: String): String =
        Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).body.subject
}