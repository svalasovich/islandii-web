package com.islandii.web.component.security.impl

import com.islandii.web.component.security.JWTProvider
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JWTAuthorizationFilter(
    authenticationManager: AuthenticationManager,
    private val jwtProvider: JWTProvider,
    private val userDetailsService: UserDetailsService
) : BasicAuthenticationFilter(authenticationManager) {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        request.getHeader(HttpHeaders.AUTHORIZATION)
            ?.takeIf { it.startsWith(JWTAuthenticationFilter.TOKEN_TYPE) }
            ?.removePrefix(JWTAuthenticationFilter.TOKEN_TYPE)
            ?.trim()
            ?.let { getAuthentication(it) }
            ?.also { SecurityContextHolder.getContext().authentication = it }

        filterChain.doFilter(request, response)
    }

    private fun getAuthentication(token: String): Authentication? {
        if (jwtProvider.validateToken(token)) {
            return jwtProvider.getUsernameFromToken(token)
                .let {
                    try {
                        userDetailsService.loadUserByUsername(it)
                    } catch (e: UsernameNotFoundException) {
                        return null
                    }
                }.let { UsernamePasswordAuthenticationToken(it.username, null, it.authorities) }
        }
        return null
    }
}