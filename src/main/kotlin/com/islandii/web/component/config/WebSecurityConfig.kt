package com.islandii.web.component.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.islandii.web.component.security.JWTProvider
import com.islandii.web.component.security.impl.JWTAuthenticationFilter
import com.islandii.web.component.security.impl.JWTAuthorizationFilter
import com.islandii.web.component.security.impl.UserDetailsServiceImpl
import com.islandii.web.repository.UserRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.config.web.servlet.invoke
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
class WebSecurityConfig(
    private val objectMapper: ObjectMapper,
    private val jwtProvider: JWTProvider,
    private val userRepository: UserRepository
) : WebSecurityConfigurerAdapter() {

    @Bean
    fun jwtAuthenticationFilter() =
        JWTAuthenticationFilter(authenticationManagerBean(), objectMapper, jwtProvider)

    @Bean
    fun jwtAuthorizationFilter() =
        JWTAuthorizationFilter(authenticationManagerBean(), jwtProvider, userDetailsServiceBean())

    override fun configure(http: HttpSecurity) {
        http {
            cors { }
            addFilterAt(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter::class.java)
            addFilterAt(jwtAuthorizationFilter(), UsernamePasswordAuthenticationFilter::class.java)
            sessionManagement { sessionCreationPolicy = SessionCreationPolicy.STATELESS }
            httpBasic { disable() }
            csrf { disable() }
            authorizeRequests {
                authorize("/actuator/health", permitAll)
                //authorize("/swagger-ui/**", authenticated)
                //authorize("/api-docs/**", authenticated)
                authorize("/actuator/**", authenticated)
                authorize("/api", authenticated)
                authorize(AntPathRequestMatcher("/**", HttpMethod.GET.name), permitAll)
                authorize(AntPathRequestMatcher("/**", HttpMethod.HEAD.name), permitAll)
                authorize(anyRequest, authenticated)
            }
//            authorizeRequests{
//                authorize(anyRequest, permitAll)
//            }
        }
    }

    @Bean
    override fun userDetailsServiceBean() =
        UserDetailsServiceImpl(userRepository, passwordEncoder())

    @Bean
    override fun authenticationManagerBean() = super.authenticationManagerBean()

    @Bean
    fun passwordEncoder() = PasswordEncoderFactories.createDelegatingPasswordEncoder()
}
