package com.islandii.web.component.config

import com.islandii.web.model.projection.HotelProjection
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer

@EnableJpaAuditing
@Configuration
class RepositoryRestConfig : RepositoryRestConfigurer {

    override fun configureRepositoryRestConfiguration(config: RepositoryRestConfiguration) {
        config.projectionConfiguration.addProjection(HotelProjection::class.java)
        config.corsRegistry.addMapping("/**")
            .allowedOrigins("*")
            .allowedMethods("*")
    }


}