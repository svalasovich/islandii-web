package com.islandii.web.component.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.projection.CollectionAwareProjectionFactory

@Configuration
class ProjectionConfig {

    @Bean
    fun projectionFactory() = CollectionAwareProjectionFactory()
}