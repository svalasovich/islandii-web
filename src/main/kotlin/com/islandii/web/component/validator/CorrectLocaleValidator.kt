package com.islandii.web.component.validator

import java.util.*
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class CorrectLocaleValidator : ConstraintValidator<CorrectLocale, Locale> {

    private val availableLocales = Locale.getAvailableLocales()
        .asSequence()
        .filter { it.country.isNotBlank() }
        .map { it.toLanguageTag() to it }
        .toMap()

    override fun isValid(value: Locale, context: ConstraintValidatorContext) =
        availableLocales.contains(value.toLanguageTag())
}