package com.islandii.web.component.validator

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.annotation.AnnotationTarget.FIELD
import kotlin.reflect.KClass


@Target(FIELD)
@Retention
@Repeatable
@Constraint(validatedBy = [CorrectLocaleValidator::class])
annotation class CorrectLocale(

    val message: String = "Locale is not valid",

    val groups: Array<KClass<*>> = [],

    val payload: Array<KClass<in Payload>> = []
)