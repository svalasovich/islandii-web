package com.islandii.web.service

import com.islandii.web.model.entity.User

interface UserService {

    fun createUser(user: User): User

}
