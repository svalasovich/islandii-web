package com.islandii.web.service

import com.islandii.web.model.dto.HotelSearch
import com.islandii.web.model.dto.HotelSearchResult
import org.springframework.data.domain.Pageable

interface HotelService {

    fun searchHotel(hotelSearch: HotelSearch, pageable: Pageable): List<HotelSearchResult>
}