package com.islandii.web.service

import com.islandii.web.model.dto.HotelRequirements
import com.islandii.web.model.entity.Hotel
import com.islandii.web.model.entity.villa.Villa
import java.math.BigDecimal
import java.time.LocalDate

interface PriceCalculatorService {

    fun calculateLowVillaPrice(
        startDate: LocalDate,
        endDate: LocalDate,
        hotelRequirements: HotelRequirements,
        hotel: Hotel
    ): Triple<Villa, BigDecimal, Int>

}