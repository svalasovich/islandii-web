package com.islandii.web.service.impl

import com.islandii.web.model.dto.HotelRequirements
import com.islandii.web.model.entity.Hotel
import com.islandii.web.model.entity.villa.Villa
import com.islandii.web.service.PriceCalculatorService
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDate
import kotlin.random.Random

@Service
class PriceCalculatorServiceImpl : PriceCalculatorService {

    override fun calculateLowVillaPrice(
        startDate: LocalDate,
        endDate: LocalDate,
        hotelRequirements: HotelRequirements,
        hotel: Hotel
    ): Triple<Villa, BigDecimal, Int> {
        return Triple(hotel.villas.first(), random(), Random.nextInt(0, 50))
    }

    private fun random() =
        BigDecimal.valueOf(Random.nextDouble(100.0, 10000.0))

}