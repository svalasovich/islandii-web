package com.islandii.web.service.impl

import com.islandii.web.model.dto.tripadvisor.LastReviews
import com.islandii.web.model.dto.tripadvisor.RatingSummary
import com.islandii.web.model.dto.tripadvisor.TravellerRating
import com.islandii.web.model.dto.tripadvisor.TripAdvisor
import com.islandii.web.service.TripAdvisorService
import org.springframework.stereotype.Service
import java.time.LocalDate
import kotlin.random.Random

@Service
class
TripAdvisorServiceImpl : TripAdvisorService {

    override fun getTripAdvisor(id: Long) = TripAdvisor(
        Random.nextDouble(0.1, 5.0),
        Random.nextLong(100, 5000),
        "https://someurl.com",
        listOf(
            RatingSummary("Quality", 5.0),
            RatingSummary("Location", 4.0),
            RatingSummary("Rooms", 4.2),
            RatingSummary("Service", 3.2),
            RatingSummary("Value", 5.0),
            RatingSummary("Cleanliness", 4.6)
        ),
        listOf(
            TravellerRating("Excellent", Random.nextInt(100, 5000)),
            TravellerRating("Very good", Random.nextInt(100, 5000)),
            TravellerRating("Average", Random.nextInt(100, 5000)),
            TravellerRating("Poor", Random.nextInt(100, 5000)),
            TravellerRating("Terrible", Random.nextInt(100, 5000))
        ),
        listOf(
            LastReviews(
                4,
                "Long weekend",
                """Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    | Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    | when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                    | It has survived not only five centuries, but also the leap into electronic typesetting, 
                    | remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                    | sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                    | Aldus PageMaker including versions of Lorem Ipsum.""".trimMargin(),
                LocalDate.now(),
                "Sergey",
                "Minsk, Belarus"
            ),
            LastReviews(
                5,
                "Long weekend2",
                """Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    | Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    | when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                    | It has survived not only five centuries, but also the leap into electronic typesetting, 
                    | remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                    | sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                    | Aldus PageMaker including versions of Lorem Ipsum.""".trimMargin(),
                LocalDate.now(),
                "Peter",
                "New Jersey, USA"
            ),
            LastReviews(
                4,
                "Long weekend3",
                """Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    | Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    | when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                    | It has survived not only five centuries, but also the leap into electronic typesetting, 
                    | remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                    | sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                    | Aldus PageMaker including versions of Lorem Ipsum.""".trimMargin(),
                LocalDate.now(),
                "Vladimir",
                "Moscow, Russia"
            ),
            LastReviews(
                2,
                "Weekend",
                """Terrible!!""".trimMargin(),
                LocalDate.now(),
                "Alexey",
                "Mogilev, Belarus"
            ),
            LastReviews(
                5,
                "Best weekend",
                """Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    | Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    | when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                    | It has survived not only five centuries, but also the leap into electronic typesetting, 
                    | remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                    | sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                    | Aldus PageMaker including versions of Lorem Ipsum.""".trimMargin(),
                LocalDate.now(),
                "Sergey",
                "Minsk, Belarus"
            )
        )
    )

}