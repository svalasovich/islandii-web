package com.islandii.web.service.impl

import com.islandii.web.model.dto.HotelSearch
import com.islandii.web.model.dto.HotelSearchResult
import com.islandii.web.repository.HotelRepository
import com.islandii.web.service.HotelService
import com.islandii.web.service.PriceCalculatorService
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import kotlin.streams.toList

@Service
class HotelServiceImpl(
    private val hotelRepository: HotelRepository,
    private val priceCalculatorService: PriceCalculatorService
) : HotelService {

    @Transactional(readOnly = true)
    override fun searchHotel(
        hotelSearch: HotelSearch,
        pageable: Pageable
    ): List<HotelSearchResult> {
        return hotelRepository.searchHotel(
            hotelSearch.startDate,
            hotelSearch.endDate,
            hotelSearch.hotelRequirements,
            hotelSearch.villaTypes,
            hotelSearch.transferTypes,
            hotelSearch.mealTypes,
            pageable.sort
        ).use { stream ->
            stream.map { hotel ->
                priceCalculatorService.calculateLowVillaPrice(
                    hotelSearch.startDate,
                    hotelSearch.endDate,
                    hotelSearch.hotelRequirements,
                    hotel
                ).let {
                    HotelSearchResult(it.first, it.second, it.third, hotel)
                }
            }
                .filter { if (hotelSearch.anyBudget) true else it.price in hotelSearch.startBudget..hotelSearch.endBudget }
                .toList()
        }
    }
}