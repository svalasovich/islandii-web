package com.islandii.web.service.impl

import com.islandii.web.model.entity.User
import com.islandii.web.repository.UserRepository
import com.islandii.web.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.ResponseStatus
import javax.persistence.EntityExistsException

@Service
class UserServiceImpl(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder
) : UserService {

    @PreAuthorize("authenticated")
    @Transactional
    override fun createUser(user: User) =
        if (userRepository.findByUsername(user.username) == null) {
            userRepository.save(
                User(user.username, user.email, passwordEncoder.encode(user.password))
            )
        } else {
            throw UserAlreadyExists("User already exists with username: ${user.username}")
        }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private class UserAlreadyExists(message: String) : EntityExistsException(message)
}