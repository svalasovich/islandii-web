package com.islandii.web.service

import com.islandii.web.model.dto.tripadvisor.TripAdvisor

interface TripAdvisorService {

    fun getTripAdvisor(id: Long): TripAdvisor
}