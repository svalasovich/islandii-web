package com.islandii.web.repository

import com.islandii.web.model.entity.Hotel
import com.islandii.web.repository.custom.impl.HotelRepositoryCustom
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface HotelRepository : JpaRepository<Hotel, Long>, HotelRepositoryCustom