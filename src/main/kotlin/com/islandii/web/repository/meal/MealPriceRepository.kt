package com.islandii.web.repository.meal

import com.islandii.web.model.entity.meal.MealPrice
import org.springframework.data.jpa.repository.JpaRepository

interface MealPriceRepository : JpaRepository<MealPrice, Long>