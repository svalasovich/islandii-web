package com.islandii.web.repository.meal

import com.islandii.web.model.entity.meal.Meal
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface MealRepository : JpaRepository<Meal, Long>