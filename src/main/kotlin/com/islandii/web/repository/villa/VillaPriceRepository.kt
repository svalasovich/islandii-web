package com.islandii.web.repository.villa

import com.islandii.web.model.entity.villa.VillaPrice
import org.springframework.data.jpa.repository.JpaRepository

interface VillaPriceRepository : JpaRepository<VillaPrice, Long>