package com.islandii.web.repository.villa

import com.islandii.web.model.entity.villa.VillaType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface VillaTypeRepository : JpaRepository<VillaType, Long>