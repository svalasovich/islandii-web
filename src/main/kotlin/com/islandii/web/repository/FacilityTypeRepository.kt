package com.islandii.web.repository

import com.islandii.web.model.entity.FacilityType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface FacilityTypeRepository : JpaRepository<FacilityType, Long>