package com.islandii.web.repository

import com.islandii.web.model.entity.Inspiration
import com.islandii.web.repository.custom.impl.InspirationRepositoryCustom
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface InspirationRepository : JpaRepository<Inspiration, Long>, InspirationRepositoryCustom