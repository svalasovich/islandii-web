package com.islandii.web.repository.custom.impl

import com.islandii.web.model.entity.Inspiration
import com.islandii.web.model.entity.localized.InspirationLocalized
import com.islandii.web.model.value.LocalizedId
import com.islandii.web.repository.custom.AbstractLocalizedRepositoryCustom
import com.islandii.web.repository.custom.LocalizedRepositoryCustom
import org.springframework.stereotype.Repository
import java.util.*
import javax.persistence.EntityManager


interface InspirationRepositoryCustom : LocalizedRepositoryCustom<Inspiration>

@Repository
class InspirationRepositoryCustomImpl(entityManager: EntityManager) :
    AbstractLocalizedRepositoryCustom<Inspiration, InspirationLocalized>(
        entityManager,
        Inspiration::class.java
    ), InspirationRepositoryCustom {

    override fun generateEntity(
        entity: Inspiration,
        localizations: Map<Locale, InspirationLocalized>
    ) = Inspiration(localizations, entity.id)

    override fun generateLocalized(
        result: Inspiration,
        value: InspirationLocalized,
        locale: Locale
    ) = InspirationLocalized(value.name, value.text, result, LocalizedId(locale))

    override fun getLocalizations(entity: Inspiration) = entity.localizations
}