package com.islandii.web.repository.custom

import org.springframework.data.jpa.repository.support.SimpleJpaRepository
import java.util.*
import javax.persistence.EntityManager

abstract class AbstractLocalizedRepositoryCustom<E, L>(
    entityManager: EntityManager,
    clazz: Class<E>
) : SimpleJpaRepository<E, Long>(clazz, entityManager) {

    @Suppress("UNCHECKED_CAST")
    override fun <S : E> save(entity: S): S {
        val localizations = mutableMapOf<Locale, L>()
        val result = generateEntity(entity, localizations)
        getLocalizations(entity).map {
            val value = it.value
            it.key to generateLocalized(result, value, it.key)
        }.also {
            localizations.putAll(it)
        }
        return super.save(result) as S
    }

    protected abstract fun generateEntity(entity: E, localizations: Map<Locale, L>): E

    protected abstract fun generateLocalized(result: E, value: L, locale: Locale): L

    protected abstract fun getLocalizations(entity: E): Map<Locale, L>
}