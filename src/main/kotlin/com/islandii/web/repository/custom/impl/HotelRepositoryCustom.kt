package com.islandii.web.repository.custom.impl

import com.islandii.web.model.dto.HotelRequirements
import com.islandii.web.model.entity.Hotel
import com.islandii.web.model.entity.Hotel_
import com.islandii.web.model.entity.localized.HotelLocalized
import com.islandii.web.model.entity.meal.Meal_
import com.islandii.web.model.entity.transfer.TransferPrice
import com.islandii.web.model.entity.transfer.TransferPrice_
import com.islandii.web.model.entity.transfer.Transfer_
import com.islandii.web.model.entity.villa.Villa
import com.islandii.web.model.entity.villa.VillaPrice_
import com.islandii.web.model.entity.villa.Villa_
import com.islandii.web.model.value.LocalizedId
import com.islandii.web.repository.custom.AbstractLocalizedRepositoryCustom
import com.islandii.web.repository.custom.LocalizedRepositoryCustom
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.rest.core.annotation.RestResource
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate
import java.time.Period
import java.util.*
import java.util.stream.Stream
import javax.persistence.EntityManager
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Fetch
import javax.persistence.criteria.Join

interface HotelRepositoryCustom : LocalizedRepositoryCustom<Hotel> {

    @RestResource(exported = false)
    fun searchHotel(
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) startDate: LocalDate,
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) endDate: LocalDate,
        hotelRequirements: HotelRequirements,
        villaTypes: Set<Long>,
        transferTypes: Set<Long>,
        mealTypes: Set<Long>,
        sort: Sort
    ): Stream<Hotel>

}

@Repository
class HotelRepositoryCustomImpl(entityManager: EntityManager) :
    AbstractLocalizedRepositoryCustom<Hotel, HotelLocalized>(entityManager, Hotel::class.java), HotelRepositoryCustom {

    override fun generateEntity(entity: Hotel, localizations: Map<Locale, HotelLocalized>) =
        Hotel(
            localizations, entity.address, entity.stars, entity.images, entity.transfers, entity.meals,
            entity.facilityTypes, entity.restaurants, entity.villas, entity.childAge, entity.babyAge, entity.id
        )

    override fun generateLocalized(result: Hotel, value: HotelLocalized, locale: Locale) =
        HotelLocalized(
            value.name, value.overview, value.special, value.facilityDescription,
            value.foodAndDrinks, value.addressName, result, LocalizedId(locale)
        )

    override fun getLocalizations(entity: Hotel): Map<Locale, HotelLocalized> = entity.localizations

    @Transactional(propagation = Propagation.MANDATORY)
    override fun searchHotel(
        startDate: LocalDate,
        endDate: LocalDate,
        hotelRequirements: HotelRequirements,
        villaTypes: Set<Long>,
        transferTypes: Set<Long>,
        mealTypes: Set<Long>,
        sort: Sort
    ): Stream<Hotel> {
        return getQuery(generateSpecification(startDate, endDate, hotelRequirements), sort)
            .resultStream
    }

    private fun generateSpecification(
        startDate: LocalDate,
        endDate: LocalDate,
        hotelRequirements: HotelRequirements
    ) =
        getStartSpecification(startDate, endDate)
            .and(getVillasSpecification(hotelRequirements, startDate, endDate))

    private fun getStartSpecification(startDate: LocalDate, endDate: LocalDate) =
        Specification<Hotel> { root, query, criteriaBuilder ->
            root.apply {
                fetch(Hotel_.localizations)
                fetch(Hotel_.meals).fetch(Meal_.mealType)
            }

            val villas = root.fetch(Hotel_.villas).apply {
                fetch(Villa_.localizations)
                fetch(Villa_.villaType)
            }.castToJoin()
            val villaPrices = villas.fetch(Villa_.villaPrices).castToJoin()
            val transfer = root.fetch(Hotel_.transfers).apply {
                fetch(Transfer_.transferType)
                fetch(Transfer_.transferPrices)
            }.castToJoin()

            val transferSubQuery = query.distinct(true).subquery(Long::class.java).run {
                val subTransferPrice = from(TransferPrice::class.java)
                val transferId = subTransferPrice.get(TransferPrice_.transfer).get(Transfer_.id)
                select(subTransferPrice.get(TransferPrice_.transfer).get(Transfer_.id))
                    .where(
                        criteriaBuilder.`in`(subTransferPrice.get(TransferPrice_.date)).value(
                            startDate
                        ).value(endDate)
                    )
                    .groupBy(transferId)
                    .having(
                        criteriaBuilder.ge(
                            criteriaBuilder.count(
                                subTransferPrice.get(
                                    TransferPrice_.id
                                )
                            ), 2
                        )
                    )
            }
            val villaPriceSubQuery = query.getVillaIdsSubQuery(criteriaBuilder, startDate, endDate)

            //ToDO mealPrices add of fetch
            criteriaBuilder.and(
                criteriaBuilder.`in`(transfer.get(Transfer_.id)).value(transferSubQuery),
                criteriaBuilder.`in`(villas.get(Villa_.id)).value(villaPriceSubQuery),
                criteriaBuilder.between(
                    villaPrices.get(VillaPrice_.date), startDate, endDate.minusDays(
                        1
                    )
                )
            )

        }

    private fun getVillasSpecification(
        hotelRequirements: HotelRequirements,
        startDate: LocalDate,
        endDate: LocalDate
    ) =
        Specification<Hotel> { root, query, criteriaBuilder ->
            val maxPlacesSubQuery = query.subquery(Long::class.java).run {
                val villaIdsSubQuery = query.getVillaIdsSubQuery(
                    criteriaBuilder,
                    startDate,
                    endDate
                )
                val villa = from(Villa::class.java)
                val hotelId = villa.get(Villa_.hotel).get(Hotel_.id)
                val adultPlaces = criteriaBuilder.sum(
                    villa.get(Villa_.adultPlaceQuantity),
                    villa.get(Villa_.occasionalAdultPlaceQuantity)
                ).let { criteriaBuilder.prod(it, villa.get(Villa_.number)) }
                    .let(criteriaBuilder::sum)
                val childPlaces = criteriaBuilder.sum(
                    villa.get(Villa_.childPlaceQuantity),
                    villa.get(Villa_.occasionalChildPlaceQuantity)
                ).let { criteriaBuilder.prod(it, villa.get(Villa_.number)) }
                    .let(criteriaBuilder::sum)

                select(hotelId)
                    .where(criteriaBuilder.`in`(villa.get(Villa_.id)).value(villaIdsSubQuery))
                    .groupBy(hotelId)
                    .having(
                        criteriaBuilder.and(
                            criteriaBuilder.ge(adultPlaces, hotelRequirements.adultsNumber),
                            criteriaBuilder.ge(
                                criteriaBuilder.sum(
                                    childPlaces, criteriaBuilder.diff(
                                        adultPlaces,
                                        hotelRequirements.adultsNumber
                                    )
                                ), hotelRequirements.childrenNumber
                            )
                        )
                    )
            }

            criteriaBuilder.`in`(root.get(Hotel_.id)).value(maxPlacesSubQuery)
        }

    private fun CriteriaQuery<*>.getVillaIdsSubQuery(
        criteriaBuilder: CriteriaBuilder,
        startDate: LocalDate,
        endDate: LocalDate
    ) = subquery(Long::class.java).run {
        val period = Period.between(startDate, endDate).days
        val subVillas = from(Villa::class.java)
        val from = subVillas.join(Villa_.villaPrices)
        val villaId = subVillas.get(Villa_.id)

        select(villaId)
            .where(
                criteriaBuilder.between(from.get(VillaPrice_.date), startDate, endDate.minusDays(1))
            )
            .groupBy(villaId)
            .having(criteriaBuilder.equal(criteriaBuilder.count(from.get(VillaPrice_.id)), period))
    }

    @Suppress("UNCHECKED_CAST")
    private fun <X, Y> Fetch<X, Y>.castToJoin() = this as Join<X, Y>
}

