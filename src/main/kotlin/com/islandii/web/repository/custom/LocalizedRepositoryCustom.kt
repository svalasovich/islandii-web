package com.islandii.web.repository.custom

interface LocalizedRepositoryCustom<S> {
    fun <T : S> save(entity: T): S
}