package com.islandii.web.repository.transfer

import com.islandii.web.model.entity.transfer.TransferPrice
import org.springframework.data.jpa.repository.JpaRepository

interface TransferPriceRepository : JpaRepository<TransferPrice, Long>