package com.islandii.web.repository.transfer

import com.islandii.web.model.entity.transfer.TransferType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface TransferTypeRepository : JpaRepository<TransferType, Long>