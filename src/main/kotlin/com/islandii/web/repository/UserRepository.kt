package com.islandii.web.repository

import com.islandii.web.model.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

@RepositoryRestResource(exported = false)
interface UserRepository : JpaRepository<User, Long> {

    fun findByUsername(username: String): User?
}