package com.islandii.web.repository

import com.islandii.web.model.entity.Restaurant
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface RestaurantRepository : JpaRepository<Restaurant, Long>