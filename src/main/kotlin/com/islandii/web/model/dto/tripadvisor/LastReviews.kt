package com.islandii.web.model.dto.tripadvisor

import java.time.LocalDate


data class LastReviews(

    val rating: Int,

    val name: String,

    val text: String,

    val date: LocalDate,

    val user: String,

    val place: String
)