package com.islandii.web.model.dto

data class Credentials(
    val username: String,
    val password: String
)