package com.islandii.web.model.dto

import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.time.LocalDate
import javax.validation.Valid
import javax.validation.constraints.PositiveOrZero

data class HotelSearch(
    @get: PositiveOrZero
    val startBudget: BigDecimal = BigDecimal.ZERO,
    @get: PositiveOrZero
    val endBudget: BigDecimal = BigDecimal.ZERO,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    //@get: FutureOrPresent
    val startDate: LocalDate,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    //@get: FutureOrPresent
    val endDate: LocalDate,
    @get: Valid
    val hotelRequirements: HotelRequirements,
    val anyBudget: Boolean = false,
    val villaTypes: Set<Long> = emptySet(),
    val transferTypes: Set<Long> = emptySet(),
    val mealTypes: Set<Long> = emptySet()
)