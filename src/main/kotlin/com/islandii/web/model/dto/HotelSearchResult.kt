package com.islandii.web.model.dto

import com.islandii.web.model.entity.Hotel
import com.islandii.web.model.entity.villa.Villa
import java.math.BigDecimal

data class HotelSearchResult(

    val villa: Villa,

    val price: BigDecimal,

    val discount: Int,

    val hotel: Hotel
)