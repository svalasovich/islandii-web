package com.islandii.web.model.dto.tripadvisor

data class TripAdvisor(

    val rating: Double,

    val reviewsNumber: Long,

    val url: String,

    val ratingSummary: List<RatingSummary>,

    val travellerRating: List<TravellerRating>,

    val lastReviews: List<LastReviews>
)