package com.islandii.web.model.dto.tripadvisor

data class TravellerRating(

    val name: String,

    val number: Int

)
