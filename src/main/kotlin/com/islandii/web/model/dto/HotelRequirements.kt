package com.islandii.web.model.dto

import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero

data class HotelRequirements(

    @get: PositiveOrZero
    val adultsNumber: Int = 0,
    @get: PositiveOrZero
    val childrenNumber: Int = 0,
    @get: Positive
    val roomNumber: Int = 0

)