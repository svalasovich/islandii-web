package com.islandii.web.model.dto.tripadvisor

data class RatingSummary(

    val name: String,

    val rating: Double

)