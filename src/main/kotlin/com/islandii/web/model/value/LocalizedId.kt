package com.islandii.web.model.value

import com.islandii.web.component.validator.CorrectLocale
import org.apache.logging.log4j.util.Strings
import java.io.Serializable
import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class LocalizedId(

    @field: CorrectLocale
    @Column(nullable = false, updatable = false)
    val locale: Locale = Locale.US,

    @Column(nullable = false, updatable = false)
    val id: Long = 0

) : Serializable