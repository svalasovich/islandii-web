package com.islandii.web.model.entity

import com.islandii.web.model.entity.localized.HotelLocalized
import com.islandii.web.model.entity.meal.Meal
import com.islandii.web.model.entity.transfer.Transfer
import com.islandii.web.model.entity.villa.Villa
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.URL
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Positive

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "htl_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "htl_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "htl_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "htl_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "htl_upd_by", nullable = false))
)
@Table(name = "isl_hotel")
class Hotel(

    @OneToMany(
        mappedBy = "hotel",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    val localizations: Map<Locale, HotelLocalized> = mapOf(),

    @field: [Length(max = 255) URL NotBlank]
    @Column(nullable = false, name = "htl_address")
    val address: String,

    @field: Positive
    @Column(nullable = false, name = "htl_stars")
    val stars: Int,

    @Column(nullable = false, length = 2048, name = "htl_images")
    val images: Set<String> = setOf(),

    @OneToMany(
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true,
        mappedBy = "hotel"
    )
    val transfers: Set<Transfer> = setOf(),

    @OneToMany(
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true,
        mappedBy = "hotel"
    )
    val meals: Set<Meal> = setOf(),

    @ManyToMany
    @JoinTable(
        name = "isl_hotel_to_facility_type",
        joinColumns = [JoinColumn(name = "htl_id")],
        inverseJoinColumns = [JoinColumn(name = "ftp_id")]
    )
    val facilityTypes: Set<FacilityType> = setOf(),

    @ManyToMany
    @JoinTable(
        name = "isl_hotel_to_restaurant",
        joinColumns = [JoinColumn(name = "htl_id")],
        inverseJoinColumns = [JoinColumn(name = "rst_id")]
    )
    val restaurants: Set<Restaurant> = setOf(),

    @OneToMany(
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true,
        mappedBy = "hotel"
    )
    val villas: Set<Villa> = setOf(),

    @field: Positive
    @Column(nullable = false, name = "htl_child_age")
    val childAge: Int = 0,

    @field: Positive
    @Column(nullable = false, name = "htl_baby_age")
    val babyAge: Int = 0,

    id: Long = 0

) : AbstractEntity(id)