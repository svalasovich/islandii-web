package com.islandii.web.model.entity.localized

import com.islandii.web.model.entity.villa.VillaType
import com.islandii.web.model.value.LocalizedId
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Immutable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@AttributeOverrides(
    AttributeOverride(
        name = "localizedId.locale",
        column = Column(name = "vtl_locale")
    )
)
@Table(name = "isl_villa_type_loc")
class VillaTypeLocalized(

    @field: [NotBlank Length(max = 255)]
    @Column(unique = true, nullable = false, name = "vtl_name")
    val name: String,

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "vlt_id", nullable = false)
    val villaType: VillaType?,

    override val localizedId: LocalizedId = LocalizedId()

) : AbstractLocalizedEntity()