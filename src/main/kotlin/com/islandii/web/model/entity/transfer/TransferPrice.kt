package com.islandii.web.model.entity.transfer

import com.islandii.web.model.entity.AbstractEntity
import org.hibernate.annotations.Immutable
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import javax.validation.constraints.FutureOrPresent
import javax.validation.constraints.Positive

@Entity
@Immutable
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "trp_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "trp_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "trp_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "trp_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "trp_upd_by", nullable = false))
)
@Table(
    name = "isl_transfer_price",
    uniqueConstraints = [UniqueConstraint(columnNames = ["trf_id", "trp_date"])]
)
class TransferPrice(

    @field: Positive
    @Column(nullable = false, updatable = false, name = "trp_value")
    val value: BigDecimal = BigDecimal.ZERO,

    @field: Positive
    @Column(nullable = false, updatable = false, name = "trp_child_value")
    val childValue: BigDecimal = BigDecimal.ZERO,

    @field: FutureOrPresent
    @Column(nullable = false, updatable = false, name = "trp_date")
    val date: LocalDate = LocalDate.MIN,

    @Column(nullable = false, updatable = false, name = "trp_currency")
    val currency: Currency = Currency.getInstance("USD"),

    @ManyToOne
    @JoinColumn(
        nullable = false,
        updatable = false,
        name = "trf_id"
    )
    val transfer: Transfer,

    id: Long = 0

) : AbstractEntity(id)