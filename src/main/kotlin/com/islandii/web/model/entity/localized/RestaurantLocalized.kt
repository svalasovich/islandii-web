package com.islandii.web.model.entity.localized

import com.islandii.web.model.entity.Restaurant
import com.islandii.web.model.value.LocalizedId
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Immutable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@AttributeOverrides(
    AttributeOverride(
        name = "localizedId.locale",
        column = Column(name = "rsl_locale")
    )
)
@Table(name = "isl_restaurant_loc")
class RestaurantLocalized(

    @field: [NotBlank Length(max = 255)]
    @Column(unique = true, nullable = false, name = "rsl_name")
    val name: String,

    @field: [NotBlank Length(max = 2560)]
    @Column(nullable = false, length = 2560, name = "rsl_description")
    val description: String,

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "rst_id", nullable = false)
    val restaurant: Restaurant?,

    override val localizedId: LocalizedId = LocalizedId()

) : AbstractLocalizedEntity()