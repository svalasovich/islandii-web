package com.islandii.web.model.entity.villa

import com.islandii.web.model.entity.AbstractEntity
import com.islandii.web.model.entity.Hotel
import com.islandii.web.model.entity.localized.VillaLocalized
import com.islandii.web.model.entity.meal.MealType
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "vil_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "vil_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "vil_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "vil_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "vil_upd_by", nullable = false))
)
@Table(name = "isl_villa")
class Villa(

    @OneToMany(
        mappedBy = "villa",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    val localizations: Map<Locale, VillaLocalized> = mapOf(),

    @Column(nullable = false, length = 2048, name = "vil_images")
    val images: Set<String> = setOf(),

    @OneToMany(
        mappedBy = "villa",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    val villaPrices: Set<VillaPrice> = setOf(),

    @field: Positive
    @Column(nullable = false, name = "vil_adult_place_quantity")
    val adultPlaceQuantity: Int = 0,

    @field: PositiveOrZero
    @Column(nullable = false, name = "vil_occasional_adult_place_quantity")
    val occasionalAdultPlaceQuantity: Int = 0,

    @field: PositiveOrZero
    @Column(nullable = false, name = "vil_child_place_quantity")
    val childPlaceQuantity: Int = 0,

    @field: PositiveOrZero
    @Column(nullable = false, name = "vil_occasional_child_place_quantity")
    val occasionalChildPlaceQuantity: Int = 0,

    @field: Positive
    @Column(nullable = false, name = "vil_number")
    val number: Int = 0,

    @ManyToOne
    @JoinColumn(nullable = false, name="vil_meal_type")
    val mealType: MealType,

    @ManyToOne
    @JoinColumn(nullable = false, name = "vlt_id")
    val villaType: VillaType,

    @ManyToOne
    @JoinColumn(nullable = false, name = "htl_id")
    val hotel: Hotel,

    id: Long = 0

) : AbstractEntity(id)