package com.islandii.web.model.entity.meal

import com.islandii.web.model.entity.AbstractEntity
import com.islandii.web.model.entity.Hotel
import javax.persistence.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "mel_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "mel_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "mel_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "mel_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "mel_upd_by", nullable = false))
)
@Table(
    name = "isl_meal",
    uniqueConstraints = [UniqueConstraint(columnNames = ["htl_id", "mlt_id"])]
)
class Meal(

    @OneToMany(
        mappedBy = "meal",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    val mealPrices: Set<MealPrice> = setOf(),

    @ManyToOne
    @JoinColumn(nullable = false, name = "mlt_id")
    val mealType: MealType,

    @ManyToOne
    @JoinColumn(nullable = false, name = "htl_id")
    val hotel: Hotel,

    id: Long = 0

) : AbstractEntity(id)