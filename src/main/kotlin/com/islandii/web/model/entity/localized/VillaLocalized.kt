package com.islandii.web.model.entity.localized

import com.islandii.web.model.entity.villa.Villa
import com.islandii.web.model.value.LocalizedId
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Immutable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@AttributeOverrides(
    AttributeOverride(
        name = "localizedId.locale",
        column = Column(name = "vll_locale")
    )
)
@Table(name = "isl_villa_loc")
class VillaLocalized(

    @field: [NotBlank Length(max = 255)]
    @Column(nullable = false, name = "vll_name")
    val name: String,

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "vil_id", nullable = false)
    val villa: Villa?,

    override val localizedId: LocalizedId = LocalizedId()

) : AbstractLocalizedEntity()