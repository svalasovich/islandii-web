package com.islandii.web.model.entity

import com.islandii.web.model.entity.localized.FacilityTypeLocalized
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.util.*
import javax.persistence.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "ftp_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "ftp_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(
        name = "lastModifiedDateTime",
        column = Column(name = "ftp_upd_date", nullable = false)
    ),
    AttributeOverride(name = "createdBy", column = Column(name = "ftp_crt_by", nullable = false, updatable = false)),
    AttributeOverride(
        name = "lastModifiedBy",
        column = Column(name = "ftp_upd_by", nullable = false)
    )
)
@Table(name = "isl_facility_type")
class FacilityType(

    @OneToMany(
        mappedBy = "facilityType",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    val localizations: Map<Locale, FacilityTypeLocalized> = mapOf(),

    id: Long = 0

) : AbstractEntity(id)