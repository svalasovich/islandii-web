package com.islandii.web.model.entity.localized

import com.islandii.web.model.value.LocalizedId
import javax.persistence.EmbeddedId
import javax.persistence.MappedSuperclass
import javax.validation.Valid

@MappedSuperclass
abstract class AbstractLocalizedEntity(

    @EmbeddedId
    @field: Valid
    open val localizedId: LocalizedId = LocalizedId()

)