package com.islandii.web.model.entity

import com.islandii.web.model.entity.localized.RestaurantLocalized
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.util.*
import javax.persistence.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "rst_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "rst_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "rst_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "rst_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "rst_upd_by", nullable = false))
)
@Table(name = "isl_restaurant")
class Restaurant(

    @OneToMany(
        mappedBy = "restaurant",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    val localizations: Map<Locale, RestaurantLocalized> = mapOf(),

    @Column(nullable = false, length = 2048, name = "rst_images")
    val images: Set<String> = setOf(),

    id: Long = 0

) : AbstractEntity(id)