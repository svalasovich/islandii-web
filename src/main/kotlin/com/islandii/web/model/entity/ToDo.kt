package com.islandii.web.model.entity

import com.islandii.web.model.entity.localized.ToDoLocalized
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.util.*
import javax.persistence.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "tdo_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "tdo_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "tdo_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "tdo_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "tdo_upd_by", nullable = false))
)
@Table(name = "isl_todo")
class ToDo(

    @OneToMany(
        mappedBy = "toDo",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    val localizations: Map<Locale, ToDoLocalized> = mapOf(),

    id: Long = 0

) : AbstractEntity(id)