package com.islandii.web.model.entity.transfer

import com.islandii.web.model.entity.AbstractEntity
import com.islandii.web.model.entity.Hotel
import javax.persistence.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "trf_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "trf_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "trf_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "trf_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "trf_upd_by", nullable = false))
)
@Table(
    name = "isl_transfer",
    uniqueConstraints = [UniqueConstraint(columnNames = ["htl_id", "trt_id"])]
)
class Transfer(

    @OneToMany(
        mappedBy = "transfer",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    val transferPrices: Set<TransferPrice> = setOf(),

    @ManyToOne
    @JoinColumn(nullable = false, name = "trt_id")
    val transferType: TransferType,

    @ManyToOne
    @JoinColumn(nullable = false, name = "htl_id")
    val hotel: Hotel,

    id: Long = 0

) : AbstractEntity(id)