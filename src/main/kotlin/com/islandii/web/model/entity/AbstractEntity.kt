package com.islandii.web.model.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.*

@JsonIgnoreProperties("createdDateTime", "lastModifiedDateTime", "createdBy", "lastModifiedBy")
@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long
) {
    @CreatedDate
    var createdDateTime: LocalDateTime = LocalDateTime.MIN
        private set

    @LastModifiedDate
    var lastModifiedDateTime: LocalDateTime = LocalDateTime.MIN
        private set

    @CreatedBy
    var createdBy: Long = 0
        private set

    @LastModifiedBy
    var lastModifiedBy: Long = 0
        private set
}