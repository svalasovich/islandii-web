package com.islandii.web.model.entity.villa

import com.islandii.web.model.entity.AbstractEntity
import org.hibernate.annotations.Immutable
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import javax.validation.constraints.FutureOrPresent
import javax.validation.constraints.Positive

@Entity
@Immutable
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "vlp_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "vlp_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "vlp_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "vlp_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "vlp_upd_by", nullable = false))
)
@Table(
    name = "isl_villa_price",
    uniqueConstraints = [UniqueConstraint(columnNames = ["vil_id", "vlp_date"])]
)
class VillaPrice(

    @field: Positive
    @Column(nullable = false, updatable = false, name = "vlp_value")
    val value: BigDecimal = BigDecimal.ZERO,

    @field: FutureOrPresent
    @Column(nullable = false, updatable = false, name = "vlp_date")
    val date: LocalDate = LocalDate.MIN,

    @Column(nullable = false, updatable = false, name = "vlp_currency")
    val currency: Currency = Currency.getInstance("USD"),

    @ManyToOne
    @JoinColumn(nullable = false, name = "vil_id")
    val villa: Villa,

    id: Long = 0

) : AbstractEntity(id)
