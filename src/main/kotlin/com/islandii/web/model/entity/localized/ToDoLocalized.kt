package com.islandii.web.model.entity.localized

import com.islandii.web.model.entity.ToDo
import com.islandii.web.model.value.LocalizedId
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Immutable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@AttributeOverrides(
    AttributeOverride(
        name = "localizedId.locale",
        column = Column(name = "tdl_locale")
    )
)
@Table(name = "isl_todo_loc")
class ToDoLocalized(
    @field: [NotBlank Length(max = 255)]
    @Column(nullable = false, unique = true, name = "tdl_name")
    val name: String,

    @field: [NotBlank Length(max = 2560)]
    @Column(nullable = false, length = 2560, name = "tdl_text")
    val text: String,

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("id")
    @JoinColumn(name = "tdo_id", nullable = false)
    val toDo: ToDo?,

    override val localizedId: LocalizedId = LocalizedId()

) : AbstractLocalizedEntity()