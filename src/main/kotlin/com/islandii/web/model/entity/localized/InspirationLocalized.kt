package com.islandii.web.model.entity.localized

import com.islandii.web.model.entity.Inspiration
import com.islandii.web.model.value.LocalizedId
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Immutable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@AttributeOverrides(
    AttributeOverride(
        name = "localizedId.locale",
        column = Column(name = "inl_locale")
    )
)
@Table(name = "isl_inspiration_loc")
class InspirationLocalized(

    @field: [NotBlank Length(max = 255)]
    @Column(nullable = false, unique = true, name = "inl_name")
    val name: String,

    @field: [NotBlank Length(max = 2560)]
    @Column(nullable = false, length = 2560, name = "inl_text")
    val text: String,

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("id")
    @JoinColumn(name = "ins_id", nullable = false)
    val inspiration: Inspiration?,

    override val localizedId: LocalizedId = LocalizedId()

) : AbstractLocalizedEntity()