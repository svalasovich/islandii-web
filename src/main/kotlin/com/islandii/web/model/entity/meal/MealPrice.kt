package com.islandii.web.model.entity.meal

import com.islandii.web.model.entity.AbstractEntity
import org.hibernate.annotations.Immutable
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import javax.validation.constraints.FutureOrPresent
import javax.validation.constraints.Positive

@Entity
@Immutable
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "mlp_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "mlp_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "mlp_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "mlp_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "mlp_upd_by", nullable = false))
)
@Table(
    name = "isl_meal_price",
    uniqueConstraints = [UniqueConstraint(columnNames = ["mel_id", "mlp_date", "mlp_preferential"])]
)
class MealPrice(

    @field: Positive
    @Column(nullable = false, updatable = false, name = "mlp_value")
    val value: BigDecimal = BigDecimal.ZERO,

    @field: FutureOrPresent
    @Column(nullable = false, updatable = false, name = "mlp_date")
    val date: LocalDate = LocalDate.MIN,

    @Column(nullable = false, updatable = false, name = "mlp_currency")
    val currency: Currency = Currency.getInstance("USD"),

    @Column(nullable = false, updatable = false, name = "mlp_preferential")
    val preferential: Boolean = false,

    @ManyToOne
    @JoinColumn(
        nullable = false,
        updatable = false,
        name = "mel_id"
    )
    val meal: Meal,

    id: Long = 0

) : AbstractEntity(id)