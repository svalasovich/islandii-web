package com.islandii.web.model.entity.localized

import com.islandii.web.model.entity.meal.MealType
import com.islandii.web.model.value.LocalizedId
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Immutable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@AttributeOverrides(
    AttributeOverride(
        name = "localizedId.locale",
        column = Column(name = "mtl_locale")
    )
)
@Table(name = "isl_meal_type_loc")
class MealTypeLocalized(

    @field: [NotBlank Length(max = 255)]
    @Column(unique = true, nullable = false, name = "mtl_name")
    val name: String,

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "mtp_id", nullable = false)
    val mealType: MealType?,

    override val localizedId: LocalizedId = LocalizedId()

) : AbstractLocalizedEntity()