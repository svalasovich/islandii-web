package com.islandii.web.model.entity.localized

import com.islandii.web.model.entity.Hotel
import com.islandii.web.model.value.LocalizedId
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Immutable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@AttributeOverrides(
    AttributeOverride(
        name = "localizedId.locale",
        column = Column(name = "hlc_locale")
    )
)
@Table(name = "isl_hotel_loc")
class HotelLocalized(

    @field: [NotBlank Length(max = 255)]
    @Column(nullable = false, unique = true, name = "hlc_name")
    val name: String,

    @field: [Length(max = 2560) NotBlank]
    @Column(nullable = false, length = 2560, name = "hlc_overview")
    val overview: String,

    @field: [Length(max = 2560) NotBlank]
    @Column(nullable = false, length = 2560, name = "hlc_special")
    val special: String,

    @field: [NotBlank Length(max = 2560)]
    @Column(nullable = false, length = 2560, name = "hlc_facility_description")
    val facilityDescription: String,

    @field: Length(max = 2560)
    @Column(nullable = false, length = 2560, name = "hlc_food_and_drinks")
    val foodAndDrinks: String,

    @field: [Length(max = 255) NotBlank]
    @Column(nullable = false, name = "hlc_address_name")
    val addressName: String,

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "htl_id", nullable = false)
    val hotel: Hotel?,

    override val localizedId: LocalizedId = LocalizedId()

) : AbstractLocalizedEntity()