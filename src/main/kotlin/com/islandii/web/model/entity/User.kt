package com.islandii.web.model.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.UniqueElements
import javax.persistence.*
import javax.validation.constraints.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "usr_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "usr_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "usr_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "usr_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "usr_upd_by", nullable = false))
)
@Table(name = "isl_user")
class User(

    @field: [NotBlank Length(min = 3, max = 20)]
    @Column(nullable = false, name = "usr_username", unique = true)
    val username: String,

    @field: Email
    @Column(nullable = false, name = "usr_email", unique = true)
    val email: String,

    @field: [NotBlank Length(min = 8)]
    @Column(nullable = false, name = "usr_password")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    val password: String,

    id: Long = 0

): AbstractEntity(id)