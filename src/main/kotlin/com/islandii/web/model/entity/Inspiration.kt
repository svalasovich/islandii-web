package com.islandii.web.model.entity

import com.islandii.web.model.entity.localized.InspirationLocalized
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.util.*
import javax.persistence.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "ins_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "ins_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "ins_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "ins_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "ins_upd_by", nullable = false))
)
@Table(name = "isl_inspiration")
class Inspiration(

    @OneToMany(
        mappedBy = "inspiration",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    val localizations: Map<Locale, InspirationLocalized> = mapOf(),

    id: Long = 0

) : AbstractEntity(id)