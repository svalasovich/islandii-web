package com.islandii.web.model.entity.localized

import com.islandii.web.model.entity.transfer.TransferType
import com.islandii.web.model.value.LocalizedId
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Immutable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@AttributeOverrides(
    AttributeOverride(
        name = "localizedId.locale",
        column = Column(name = "ttl_locale")
    )
)
@Table(name = "isl_transfer_type_loc")
class TransferTypeLocalized(

    @field: [NotBlank Length(max = 255)]
    @Column(unique = true, nullable = false, name = "ttl_name")
    val name: String,

    @ManyToOne
    @MapsId("id")
    @JoinColumn(
        name = "ttp_id",
        nullable = false
    )
    val transferType: TransferType?,

    override val localizedId: LocalizedId = LocalizedId()

) : AbstractLocalizedEntity()