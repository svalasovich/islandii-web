package com.islandii.web.model.entity.villa

import com.islandii.web.model.entity.AbstractEntity
import com.islandii.web.model.entity.localized.VillaTypeLocalized
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.util.*
import javax.persistence.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "vlt_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "vlt_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "vlt_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "vlt_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "vlt_upd_by", nullable = false))
)
@Table(name = "isl_villa_type")
class VillaType(

    @OneToMany(
        mappedBy = "villaType",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    val localizations: Map<Locale, VillaTypeLocalized> = mapOf(),

    id: Long = 0

) : AbstractEntity(id)