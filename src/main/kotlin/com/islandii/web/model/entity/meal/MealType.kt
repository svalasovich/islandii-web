package com.islandii.web.model.entity.meal

import com.islandii.web.model.entity.AbstractEntity
import com.islandii.web.model.entity.localized.MealTypeLocalized
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.util.*
import javax.persistence.*

@Entity
@AttributeOverrides(
    AttributeOverride(name = "id", column = Column(name = "mlt_id", nullable = false)),
    AttributeOverride(
        name = "createdDateTime",
        column = Column(name = "mlt_crt_date", nullable = false, updatable = false)
    ),
    AttributeOverride(name = "lastModifiedDateTime", column = Column(name = "mlt_upd_date", nullable = false)),
    AttributeOverride(name = "createdBy", column = Column(name = "mlt_crt_by", nullable = false, updatable = false)),
    AttributeOverride(name = "lastModifiedBy", column = Column(name = "mlt_upd_by", nullable = false))
)
@Table(name = "isl_meal_type")
class MealType(

    @OneToMany(
        mappedBy = "mealType",
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH],
        orphanRemoval = true
    )
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    val localizations: Map<Locale, MealTypeLocalized> = mapOf(),

    id: Long = 0

) : AbstractEntity(id)