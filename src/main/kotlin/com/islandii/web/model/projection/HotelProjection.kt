package com.islandii.web.model.projection

import com.islandii.web.model.dto.tripadvisor.LastReviews
import com.islandii.web.model.dto.tripadvisor.RatingSummary
import com.islandii.web.model.dto.tripadvisor.TravellerRating
import com.islandii.web.model.dto.tripadvisor.TripAdvisor
import com.islandii.web.model.entity.Hotel
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.rest.core.config.Projection
import java.util.*

@Projection(types = [Hotel::class], name = "hotelProjection")
interface HotelProjection {

    val localizations: Map<Locale, HotelLocalizedProjection>

    val address: String

    val stars: Int

    val images: Set<String>

    val transfers: Set<TransferProjection>

    val meals: Set<MealProjection>

    val facilityTypes: Set<FacilityTypeProjection>

    @get: Value("#{@tripAdvisorServiceImpl.getTripAdvisor(1)}")
    val tripAdvisor: TripAdvisor

    //ToDo add villas

    interface HotelLocalizedProjection {

        val name: String

        val overview: String

        val special: String

        val facilityDescription: String

        val foodAndDrinks: String
    }

    interface TransferProjection {

        val transferType: TransferTypeProjection

        interface TransferTypeProjection {
            val id: Long
        }
    }

    interface FacilityTypeProjection {

        val id: Long

        val localizations: Map<Locale, FacilityTypeLocalizedProjection>

        interface FacilityTypeLocalizedProjection {
            val name: String
        }
    }

    interface MealProjection {

        val mealType: MealTypeProjection

        interface MealTypeProjection {
            val id: Long
        }
    }

    interface TripAdvisorProjection {

        val rating: Double

        val reviewsNumber: Long

        val url: String

        val ratingSummary: List<RatingSummary>

        val travellerRating: List<TravellerRating>

        val lastReviews: List<LastReviews>
    }
}