package com.islandii.web.model.projection

import com.islandii.web.model.dto.HotelSearchResult
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.rest.core.config.Projection
import java.math.BigDecimal
import java.util.*

@Projection(types = [HotelSearchResult::class])
interface HotelSearchProjection {

    val price: BigDecimal

    val discount: Int

    val villa: VillaProjection

    val hotel: HotelProjection

    interface VillaProjection {

        val localizations: Map<Locale, LocalizationProjection>

        val mealType: MealTypeProjection

    }

    interface TripAdvisorProjection {

        val rating: Double

        val reviewsNumber: Long

        val url: String

    }

    interface HotelProjection {

        val id: Long

        val meals: Set<MealProjection>

        val transfers: Set<TransferProjection>

        val stars: Int

        val localizations: Map<Locale, LocalizationProjection>

        @get: Value("#{@tripAdvisorServiceImpl.getTripAdvisor(1)}")
        val tripAdvisor: TripAdvisorProjection

        interface TransferProjection {

            val transferType: TransferTypeProjection
        }

        interface TransferTypeProjection {

            val id: Long
        }
    }

    interface MealTypeProjection {

        val id: Long
    }

    interface MealProjection {

        val mealType: MealTypeProjection
    }

    interface LocalizationProjection {
        val name: String
    }
}

