CREATE TABLE isl_user
(
    usr_id           BIGINT auto_increment PRIMARY KEY,
    usr_crt_by       BIGINT      NOT NULL,
    usr_crt_DATE     DATETIME(6) NOT NULL,
    usr_upd_by       BIGINT      NOT NULL,
    usr_upd_DATE     DATETIME(6) NOT NULL,
    usr_username     VARCHAR(255) NOT NULL,
    usr_email        VARCHAR(255) NOT NULL,
    usr_password     VARCHAR(255) NOT NULL
);