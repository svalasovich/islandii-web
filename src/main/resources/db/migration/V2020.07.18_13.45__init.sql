CREATE TABLE isl_facility_type
(
    ftp_id       BIGINT auto_increment
        PRIMARY KEY,
    ftp_crt_by   BIGINT      NOT NULL,
    ftp_crt_DATE DATETIME(6) NOT NULL,
    ftp_upd_by   BIGINT      NOT NULL,
    ftp_upd_DATE DATETIME(6) NOT NULL
);

CREATE TABLE isl_facility_type_loc
(
    ftp_id     BIGINT       NOT NULL,
    ftl_locale VARCHAR(255) NOT NULL,
    ftl_name   VARCHAR(255) NOT NULL,
    PRIMARY KEY (ftp_id, ftl_locale),
    CONSTRAINT UK_98ubdokfxgons9m89q97l5kyx
        UNIQUE (ftl_name),
    CONSTRAINT FKdmaqneu7jo1anv0lpa4cpofaa
        FOREIGN KEY (ftp_id) REFERENCES isl_facility_type (ftp_id)
);

CREATE TABLE isl_hotel
(
    htl_id       BIGINT auto_increment
        PRIMARY KEY,
    htl_crt_by   BIGINT        NOT NULL,
    htl_crt_DATE DATETIME(6)   NOT NULL,
    htl_upd_by   BIGINT        NOT NULL,
    htl_upd_DATE DATETIME(6)   NOT NULL,
    htl_address  VARCHAR(255)  NOT NULL,
    htl_images   VARCHAR(2048) NOT NULL,
    htl_stars    INT           NOT NULL
);

CREATE TABLE isl_hotel_loc
(
    htl_id                   BIGINT        NOT NULL,
    hlc_locale               VARCHAR(255)  NOT NULL,
    hlc_facility_description VARCHAR(2560) NOT NULL,
    hlc_food_and_drinks      VARCHAR(2560) NOT NULL,
    hlc_name                 VARCHAR(255)  NOT NULL,
    hlc_overview             VARCHAR(2560) NOT NULL,
    hlc_special              VARCHAR(2560) NOT NULL,
    PRIMARY KEY (htl_id, hlc_locale),
    CONSTRAINT UK_r25juojffxkuts6gkdyyrdhiy
        UNIQUE (hlc_name),
    CONSTRAINT FK7vy3cc1g6ju2hacepa4ktpqdd
        FOREIGN KEY (htl_id) REFERENCES isl_hotel (htl_id)
);

CREATE TABLE isl_hotel_to_facility_type
(
    htl_id BIGINT NOT NULL,
    ftp_id BIGINT NOT NULL,
    PRIMARY KEY (htl_id, ftp_id),
    CONSTRAINT FK66pyflc5x4nn2xqprpiwy74to
        FOREIGN KEY (htl_id) REFERENCES isl_hotel (htl_id),
    CONSTRAINT FKtf1vio583dsfmlmhbh4y0gd9j
        FOREIGN KEY (ftp_id) REFERENCES isl_facility_type (ftp_id)
);

CREATE TABLE isl_inspiration
(
    ins_id       BIGINT auto_increment
        PRIMARY KEY,
    ins_crt_by   BIGINT      NOT NULL,
    ins_crt_DATE DATETIME(6) NOT NULL,
    ins_upd_by   BIGINT      NOT NULL,
    ins_upd_DATE DATETIME(6) NOT NULL
);

CREATE TABLE isl_inspiration_loc
(
    ins_id     BIGINT        NOT NULL,
    inl_locale VARCHAR(255)  NOT NULL,
    inl_name   VARCHAR(255)  NOT NULL,
    inl_text   VARCHAR(2560) NOT NULL,
    PRIMARY KEY (ins_id, inl_locale),
    CONSTRAINT UK_474q9si3w5e7pgvdnq7dgemsj
        UNIQUE (inl_name),
    CONSTRAINT FKc7toyoiyswvid72k5380nt6u2
        FOREIGN KEY (ins_id) REFERENCES isl_inspiration (ins_id)
);

CREATE TABLE isl_meal_type
(
    mlt_id       BIGINT auto_increment
        PRIMARY KEY,
    mlt_crt_by   BIGINT      NOT NULL,
    mlt_crt_DATE DATETIME(6) NOT NULL,
    mlt_upd_by   BIGINT      NOT NULL,
    mlt_upd_DATE DATETIME(6) NOT NULL
);

CREATE TABLE isl_meal
(
    mel_id       BIGINT auto_increment
        PRIMARY KEY,
    mel_crt_by   BIGINT      NOT NULL,
    mel_crt_DATE DATETIME(6) NOT NULL,
    mel_upd_by   BIGINT      NOT NULL,
    mel_upd_DATE DATETIME(6) NOT NULL,
    htl_id       BIGINT      NOT NULL,
    mlt_id       BIGINT      NOT NULL,
    CONSTRAINT UKhi25ylfy3l9ummicgqrfl1abe
        UNIQUE (htl_id, mlt_id),
    CONSTRAINT FKl27g1m5raim7woq8g4nrym76x
        FOREIGN KEY (mlt_id) REFERENCES isl_meal_type (mlt_id),
    CONSTRAINT FKl7igysf1q29hw3t4bpk8q6w71
        FOREIGN KEY (htl_id) REFERENCES isl_hotel (htl_id)
);

CREATE TABLE isl_meal_price
(
    mlp_id           BIGINT auto_increment
        PRIMARY KEY,
    mlp_crt_by       BIGINT         NOT NULL,
    mlp_crt_DATE     DATETIME(6)    NOT NULL,
    mlp_upd_by       BIGINT         NOT NULL,
    mlp_upd_DATE     DATETIME(6)    NOT NULL,
    mlp_currency     VARCHAR(255)   NOT NULL,
    mlp_DATE         DATE           NOT NULL,
    mlp_preferential BOOLEAN            NOT NULL,
    mlp_value        DECIMAL(19, 2) NOT NULL,
    mel_id           BIGINT         NOT NULL,
    CONSTRAINT UKlfg9n92hnokfmj83t173mgrfv
        UNIQUE (mel_id, mlp_DATE, mlp_preferential),
    CONSTRAINT FKqgbh0b75hek84sdpihtf0jt3k
        FOREIGN KEY (mel_id) REFERENCES isl_meal (mel_id)
);

CREATE TABLE isl_meal_type_loc
(
    mtp_id     BIGINT       NOT NULL,
    mtl_locale VARCHAR(255) NOT NULL,
    mtl_name   VARCHAR(255) NOT NULL,
    PRIMARY KEY (mtp_id, mtl_locale),
    CONSTRAINT UK_tacrqabei1nl32af5oj5bdvd
        UNIQUE (mtl_name),
    CONSTRAINT FKritbmeowhuvwer3ckrsky27u7
        FOREIGN KEY (mtp_id) REFERENCES isl_meal_type (mlt_id)
);

CREATE TABLE isl_restaurant
(
    rst_id       BIGINT auto_increment
        PRIMARY KEY,
    rst_crt_by   BIGINT        NOT NULL,
    rst_crt_DATE DATETIME(6)   NOT NULL,
    rst_upd_by   BIGINT        NOT NULL,
    rst_upd_DATE DATETIME(6)   NOT NULL,
    rst_images   VARCHAR(2048) NOT NULL
);

CREATE TABLE isl_hotel_to_restaurant
(
    htl_id BIGINT NOT NULL,
    rst_id BIGINT NOT NULL,
    PRIMARY KEY (htl_id, rst_id),
    CONSTRAINT FK3bw04p9wkcuyo0x1nxwd9nxuj
        FOREIGN KEY (htl_id) REFERENCES isl_hotel (htl_id),
    CONSTRAINT FK8e55531k647mvy7hvpbetabrv
        FOREIGN KEY (rst_id) REFERENCES isl_restaurant (rst_id)
);

CREATE TABLE isl_restaurant_loc
(
    rst_id          BIGINT        NOT NULL,
    rsl_locale      VARCHAR(255)  NOT NULL,
    rsl_description VARCHAR(2560) NOT NULL,
    rsl_name        VARCHAR(255)  NOT NULL,
    PRIMARY KEY (rst_id, rsl_locale),
    CONSTRAINT UK_4x104smanatb3s5irbqh7hiu4
        UNIQUE (rsl_name),
    CONSTRAINT FKeoeswts2xgy8rds1aeroui72w
        FOREIGN KEY (rst_id) REFERENCES isl_restaurant (rst_id)
);

CREATE TABLE isl_todo
(
    tdo_id       BIGINT auto_increment
        PRIMARY KEY,
    tdo_crt_by   BIGINT      NOT NULL,
    tdo_crt_DATE DATETIME(6) NOT NULL,
    tdo_upd_by   BIGINT      NOT NULL,
    tdo_upd_DATE DATETIME(6) NOT NULL
);

CREATE TABLE isl_todo_loc
(
    tdo_id     BIGINT        NOT NULL,
    tdl_locale VARCHAR(255)  NOT NULL,
    tdl_name   VARCHAR(255)  NOT NULL,
    tdl_text   VARCHAR(2560) NOT NULL,
    PRIMARY KEY (tdo_id, tdl_locale),
    CONSTRAINT UK_ncqb2iw3lrafmbnb4hqguc4nc
        UNIQUE (tdl_name),
    CONSTRAINT FKq87i4tx2vhidu97j6p6t2nbqc
        FOREIGN KEY (tdo_id) REFERENCES isl_todo (tdo_id)
);

CREATE TABLE isl_transfer_type
(
    trt_id       BIGINT auto_increment
        PRIMARY KEY,
    trt_crt_by   BIGINT      NOT NULL,
    trt_crt_DATE DATETIME(6) NOT NULL,
    trt_upd_by   BIGINT      NOT NULL,
    trt_upd_DATE DATETIME(6) NOT NULL
);

CREATE TABLE isl_transfer
(
    trf_id       BIGINT auto_increment
        PRIMARY KEY,
    trf_crt_by   BIGINT      NOT NULL,
    trf_crt_DATE DATETIME(6) NOT NULL,
    trf_upd_by   BIGINT      NOT NULL,
    trf_upd_DATE DATETIME(6) NOT NULL,
    htl_id       BIGINT      NOT NULL,
    trt_id       BIGINT      NOT NULL,
    CONSTRAINT UKsgir7brn9uy62dyogb6upl1dj
        UNIQUE (htl_id, trt_id),
    CONSTRAINT FK9u13605dq46giytlwouv8sbdp
        FOREIGN KEY (htl_id) REFERENCES isl_hotel (htl_id),
    CONSTRAINT FKgxh2mn1c8wj5mfn325kea9lp3
        FOREIGN KEY (trt_id) REFERENCES isl_transfer_type (trt_id)
);

CREATE TABLE isl_transfer_price
(
    trp_id                  BIGINT auto_increment
        PRIMARY KEY,
    trp_crt_by              BIGINT         NOT NULL,
    trp_crt_DATE            DATETIME(6)    NOT NULL,
    trp_upd_by              BIGINT         NOT NULL,
    trp_upd_DATE            DATETIME(6)    NOT NULL,
    trp_currency            VARCHAR(255)   NOT NULL,
    trp_DATE                DATE           NOT NULL,
    trp_child_value         DECIMAL(19, 2) NOT NULL,
    trp_value               DECIMAL(19, 2) NOT NULL,
    trf_id                  BIGINT         NOT NULL,
    CONSTRAINT UK5643n6jk7l90nxfppml5klcmr
        UNIQUE (trf_id, trp_DATE),
    CONSTRAINT FKbopdjuqfyw1066ieu6uoe4s0o
        FOREIGN KEY (trf_id) REFERENCES isl_transfer (trf_id)
);

CREATE TABLE isl_transfer_type_loc
(
    ttp_id     BIGINT       NOT NULL,
    ttl_locale VARCHAR(255) NOT NULL,
    ttl_name   VARCHAR(255) NOT NULL,
    PRIMARY KEY (ttp_id, ttl_locale),
    CONSTRAINT UK_mtr5lmp1sowy2og0fk48idcld
        UNIQUE (ttl_name),
    CONSTRAINT FKplegp6j0n3oxd8fgeoy76wdbf
        FOREIGN KEY (ttp_id) REFERENCES isl_transfer_type (trt_id)
);

CREATE TABLE isl_villa_type
(
    vlt_id       BIGINT auto_increment
        PRIMARY KEY,
    vlt_crt_by   BIGINT      NOT NULL,
    vlt_crt_DATE DATETIME(6) NOT NULL,
    vlt_upd_by   BIGINT      NOT NULL,
    vlt_upd_DATE DATETIME(6) NOT NULL
);

CREATE TABLE isl_villa
(
    vil_id                              BIGINT auto_increment
        PRIMARY KEY,
    vil_crt_by                          BIGINT        NOT NULL,
    vil_crt_DATE                        DATETIME(6)   NOT NULL,
    vil_upd_by                          BIGINT        NOT NULL,
    vil_upd_DATE                        DATETIME(6)   NOT NULL,
    vil_images                          VARCHAR(2048) NOT NULL,
    vil_adult_place_quantity            INT           NOT NULL,
    vil_occasional_adult_place_quantity INT           NOT NULL,
    vil_child_place_quantity            INT           NOT NULL,
    vil_occasional_child_place_quantity INT           NOT NULL,
    vil_number                          INT           NOT NULL,
    vil_meal_type                       BIGINT        NOT NULL,
    htl_id                              BIGINT        NOT NULL,
    vlt_id                              BIGINT        NOT NULL,
    CONSTRAINT FK9b0dp1stxtthvapxbub4b9n70
        FOREIGN KEY (vlt_id) REFERENCES isl_villa_type (vlt_id),
    CONSTRAINT FK9b0dp1stxtthvagfbub4b9n70
        FOREIGN KEY (vil_meal_type) REFERENCES isl_meal_type (mlt_id),
    CONSTRAINT FKb0sxf4rstjb7d460f37x2aakm
        FOREIGN KEY (htl_id) REFERENCES isl_hotel (htl_id)
);

CREATE TABLE isl_villa_loc
(
    vil_id     BIGINT       NOT NULL,
    vll_locale VARCHAR(255) NOT NULL,
    vll_name   VARCHAR(255) NOT NULL,
    PRIMARY KEY (vil_id, vll_locale),
    CONSTRAINT FK84jupyfmwwurir1oug183qjds
        FOREIGN KEY (vil_id) REFERENCES isl_villa (vil_id)
);

CREATE TABLE isl_villa_price
(
    vlp_id       BIGINT auto_increment
        PRIMARY KEY,
    vlp_crt_by   BIGINT         NOT NULL,
    vlp_crt_DATE DATETIME(6)    NOT NULL,
    vlp_upd_by   BIGINT         NOT NULL,
    vlp_upd_DATE DATETIME(6)    NOT NULL,
    vlp_currency VARCHAR(255)   NOT NULL,
    vlp_DATE     DATE           NOT NULL,
    vlp_value    DECIMAL(19, 2) NOT NULL,
    vil_id       BIGINT         NOT NULL,
    CONSTRAINT UKl856h4jgqbbfbah5wbfrvdphr
        UNIQUE (vil_id, vlp_DATE),
    CONSTRAINT FK21jaw2ei3mqxivny2aj1ljtfd
        FOREIGN KEY (vil_id) REFERENCES isl_villa (vil_id)
);

CREATE TABLE isl_villa_type_loc
(
    vlt_id     BIGINT       NOT NULL,
    vtl_locale VARCHAR(255) NOT NULL,
    vtl_name   VARCHAR(255) NOT NULL,
    PRIMARY KEY (vlt_id, vtl_locale),
    CONSTRAINT UK_ohjeb8oji6tc0rskj8ny7r6fb
        UNIQUE (vtl_name),
    CONSTRAINT FK6lr4lmmcaj6w8py4m91tb4byu
        FOREIGN KEY (vlt_id) REFERENCES isl_villa_type (vlt_id)
);