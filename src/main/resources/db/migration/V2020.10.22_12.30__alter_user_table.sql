ALTER TABLE `islandii`.`isl_user`
    ADD CONSTRAINT UK_usr_username UNIQUE (usr_username),
    ADD CONSTRAINT UK_usr_email UNIQUE (usr_email);