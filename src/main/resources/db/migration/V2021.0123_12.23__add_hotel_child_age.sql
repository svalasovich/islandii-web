ALTER TABLE `islandii`.`isl_hotel`
    ADD COLUMN `htl_child_age` INT NOT NULL AFTER `htl_stars`,
    ADD COLUMN `htl_baby_age` INT NOT NULL AFTER `htl_child_age`;
