-- Hotel
INSERT INTO islandii.isl_hotel (htl_id, htl_crt_by, htl_crt_DATE, htl_upd_by, htl_upd_DATE, htl_address, htl_images, htl_stars, htl_child_age, htl_baby_age) VALUES (2, 2, '2020-07-18 15:46:17', 2, '2020-07-18 15:46:24', 'https://goo.gl/maps/U6RbEwWJ1BiJB7QM8', '', 2, 15, 2);
INSERT INTO islandii.isl_hotel (htl_id, htl_crt_by, htl_crt_DATE, htl_upd_by, htl_upd_DATE, htl_address, htl_images, htl_stars, htl_child_age, htl_baby_age) VALUES (1, 1, '2020-07-18 15:44:47', 1, '2020-07-18 15:44:44', 'https://goo.gl/maps/Lf9UzucLVC16qtvS9', '', 3, 17, 2);
INSERT INTO islandii.isl_hotel (htl_id, htl_crt_by, htl_crt_DATE, htl_upd_by, htl_upd_DATE, htl_address, htl_images, htl_stars, htl_child_age, htl_baby_age) VALUES (3, 1, '2020-07-18 15:47:02', 1, '2020-07-18 15:47:04', 'https://goo.gl/maps/8y9ZztD2zaQJnWD6A', '', 5, 16, 2);

-- Hotel Locale
INSERT INTO islandii.isl_hotel_loc (htl_id, hlc_locale, hlc_facility_description, hlc_food_and_drinks, hlc_address_name, hlc_name, hlc_overview, hlc_special) VALUES (1, 'en-US', 'Many Facilities', 'Many Food and Drinks', 'Bla bla bla address name', 'Big Hotel', 'Overview for a Big Hotel', 'Swiming pool');
INSERT INTO islandii.isl_hotel_loc (htl_id, hlc_locale, hlc_facility_description, hlc_food_and_drinks, hlc_address_name, hlc_name, hlc_overview, hlc_special) VALUES (1, 'ru-RU', 'Много бонусов', 'Много еды и напитков', 'Бла бла бла адрес', 'Большой Отель', 'Описание для большого отеля', 'Бассейн');
INSERT INTO islandii.isl_hotel_loc (htl_id, hlc_locale, hlc_facility_description, hlc_food_and_drinks, hlc_address_name, hlc_name, hlc_overview, hlc_special) VALUES (2, 'en-US', 'Less facilities', 'Less Food and Drinks', 'Lo lo lo address name', 'Little hotel', 'Overview for Little hotel', 'There are no specials');
INSERT INTO islandii.isl_hotel_loc (htl_id, hlc_locale, hlc_facility_description, hlc_food_and_drinks, hlc_address_name, hlc_name, hlc_overview, hlc_special) VALUES (3, 'en-US', 'VIP Facilities', 'VIP Food and Drink', 'Simple address', 'VIP Hotel', 'Overview for a VIP Hotel', 'swiming pool and tennis');
INSERT INTO islandii.isl_hotel_loc (htl_id, hlc_locale, hlc_facility_description, hlc_food_and_drinks, hlc_address_name, hlc_name, hlc_overview, hlc_special) VALUES (3, 'ru-RU', 'VIP бонусы', 'VIP Еда и напитки', 'Простой адрес', 'VIP Отель', 'Описание VIP отеля', 'бассейн и тенис');

-- Facility Type
INSERT INTO islandii.isl_facility_type (ftp_id, ftp_crt_by, ftp_crt_DATE, ftp_upd_by, ftp_upd_DATE) VALUES (1, 1, '2020-07-18 16:10:06', 1, '2020-07-18 16:10:09');
INSERT INTO islandii.isl_facility_type (ftp_id, ftp_crt_by, ftp_crt_DATE, ftp_upd_by, ftp_upd_DATE) VALUES (2, 2, '2020-07-18 16:10:22', 1, '2020-07-18 16:10:25');

-- Facility Type Locale
INSERT INTO islandii.isl_facility_type_loc (ftp_id, ftl_locale, ftl_name) VALUES (2, 'en-US', 'Family rooms');
INSERT INTO islandii.isl_facility_type_loc (ftp_id, ftl_locale, ftl_name) VALUES (1, 'en-US', 'Free WiFi');
INSERT INTO islandii.isl_facility_type_loc (ftp_id, ftl_locale, ftl_name) VALUES (1, 'ru-RU', 'Бесплатный WiFi');
INSERT INTO islandii.isl_facility_type_loc (ftp_id, ftl_locale, ftl_name) VALUES (2, 'ru-RU', 'Семейные комнаты');

-- Facility type to hotel
INSERT INTO islandii.isl_hotel_to_facility_type (htl_id, ftp_id) VALUES (1, 1);
INSERT INTO islandii.isl_hotel_to_facility_type (htl_id, ftp_id) VALUES (3, 1);
INSERT INTO islandii.isl_hotel_to_facility_type (htl_id, ftp_id) VALUES (2, 2);
INSERT INTO islandii.isl_hotel_to_facility_type (htl_id, ftp_id) VALUES (3, 2);

-- Restaurant
INSERT INTO islandii.isl_restaurant (rst_id, rst_crt_by, rst_crt_DATE, rst_upd_by, rst_upd_DATE, rst_images) VALUES (1, 1, '2020-07-18 17:27:22', 1, '2020-07-18 17:27:10', '');
INSERT INTO islandii.isl_restaurant (rst_id, rst_crt_by, rst_crt_DATE, rst_upd_by, rst_upd_DATE, rst_images) VALUES (2, 1, '2020-07-18 17:27:23', 2, '2020-07-18 17:27:14', '');
INSERT INTO islandii.isl_restaurant (rst_id, rst_crt_by, rst_crt_DATE, rst_upd_by, rst_upd_DATE, rst_images) VALUES (3, 2, '2020-07-18 17:27:25', 1, '2020-07-18 17:27:15', '');
INSERT INTO islandii.isl_restaurant (rst_id, rst_crt_by, rst_crt_DATE, rst_upd_by, rst_upd_DATE, rst_images) VALUES (4, 2, '2020-07-18 17:27:27', 2, '2020-07-18 17:27:16', '');

-- Restaurant to Hotel
INSERT INTO islandii.isl_hotel_to_restaurant (htl_id, rst_id) VALUES (1, 1);
INSERT INTO islandii.isl_hotel_to_restaurant (htl_id, rst_id) VALUES (2, 2);
INSERT INTO islandii.isl_hotel_to_restaurant (htl_id, rst_id) VALUES (3, 3);
INSERT INTO islandii.isl_hotel_to_restaurant (htl_id, rst_id) VALUES (3, 4);

-- Restaurant Locale
INSERT INTO islandii.isl_restaurant_loc (rst_id, rsl_locale, rsl_description, rsl_name) VALUES (1, 'en-US', 'Good restaurant', 'Goode restaurant with good service');
INSERT INTO islandii.isl_restaurant_loc (rst_id, rsl_locale, rsl_description, rsl_name) VALUES (1, 'ru-RU', 'Хороший ресторан', 'Хороший ресторан с хорошим обслуживанием');
INSERT INTO islandii.isl_restaurant_loc (rst_id, rsl_locale, rsl_description, rsl_name) VALUES (2, 'en-US', 'Bad restaurant', 'Bad restaurant, but cheap');
INSERT INTO islandii.isl_restaurant_loc (rst_id, rsl_locale, rsl_description, rsl_name) VALUES (2, 'ru-RU', 'Плохой ресторан', 'Плохой ресторан, но дешевый');
INSERT INTO islandii.isl_restaurant_loc (rst_id, rsl_locale, rsl_description, rsl_name) VALUES (3, 'en-US', 'VIP restaurant', 'VIR restaurant with best service');
INSERT INTO islandii.isl_restaurant_loc (rst_id, rsl_locale, rsl_description, rsl_name) VALUES (3, 'ru-RU', 'VIP ресторан', 'VIP ресторан с отличным сервисом');
INSERT INTO islandii.isl_restaurant_loc (rst_id, rsl_locale, rsl_description, rsl_name) VALUES (4, 'en-US', 'Restaurant with micheline star', 'Best restaurant, but very expensive');
INSERT INTO islandii.isl_restaurant_loc (rst_id, rsl_locale, rsl_description, rsl_name) VALUES (4, 'ru-RU', 'Ресторан со звездой Мишлена', 'Лучший ресторан, но очень дорогой');

-- Inspiration
INSERT INTO islandii.isl_inspiration (ins_id, ins_crt_by, ins_crt_DATE, ins_upd_by, ins_upd_DATE) VALUES (1, 2, '2020-07-18 17:46:12', 1, '2020-07-18 17:46:17');
INSERT INTO islandii.isl_inspiration (ins_id, ins_crt_by, ins_crt_DATE, ins_upd_by, ins_upd_DATE) VALUES (2, 2, '2020-07-18 17:46:14', 1, '2020-07-18 17:46:19');

-- Inspiration Locale
INSERT INTO islandii.isl_inspiration_loc (ins_id, inl_locale, inl_name, inl_text) VALUES (1, 'en-US', 'Best place', 'Maldivies best place for relax');
INSERT INTO islandii.isl_inspiration_loc (ins_id, inl_locale, inl_name, inl_text) VALUES (1, 'ru-RU', 'Лучшее место', 'Мальдивы лучшее место для отдыха');
INSERT INTO islandii.isl_inspiration_loc (ins_id, inl_locale, inl_name, inl_text) VALUES (2, 'en-US', 'Best world', 'Earth best world');
INSERT INTO islandii.isl_inspiration_loc (ins_id, inl_locale, inl_name, inl_text) VALUES (2, 'ru-RU', 'Лучший мир', 'Земля лучший мир');

-- Meal type
INSERT INTO islandii.isl_meal_type (mlt_id, mlt_crt_by, mlt_crt_DATE, mlt_upd_by, mlt_upd_DATE) VALUES (1, 1, '2020-07-18 18:18:04', 2, '2020-07-18 18:18:09');
INSERT INTO islandii.isl_meal_type (mlt_id, mlt_crt_by, mlt_crt_DATE, mlt_upd_by, mlt_upd_DATE) VALUES (2, 1, '2020-07-18 18:18:06', 2, '2020-07-18 18:18:11');
INSERT INTO islandii.isl_meal_type (mlt_id, mlt_crt_by, mlt_crt_DATE, mlt_upd_by, mlt_upd_DATE) VALUES (3, 2, '2020-07-18 18:18:17', 1, '2020-07-18 18:18:19');
INSERT INTO islandii.isl_meal_type (mlt_id, mlt_crt_by, mlt_crt_DATE, mlt_upd_by, mlt_upd_DATE) VALUES (4, 1, '2020-07-18 18:20:44', 2, '2020-07-18 18:20:46');

-- Meal type Locale
INSERT INTO islandii.isl_meal_type_loc (mtp_id, mtl_locale, mtl_name) VALUES (4, 'en-US', 'All Inclusive');
INSERT INTO islandii.isl_meal_type_loc (mtp_id, mtl_locale, mtl_name) VALUES (1, 'en-US', 'Breakfast');
INSERT INTO islandii.isl_meal_type_loc (mtp_id, mtl_locale, mtl_name) VALUES (2, 'en-US', 'Breakfast and Dinner');
INSERT INTO islandii.isl_meal_type_loc (mtp_id, mtl_locale, mtl_name) VALUES (3, 'en-US', 'Breakfast, Lunch and Dinner');
INSERT INTO islandii.isl_meal_type_loc (mtp_id, mtl_locale, mtl_name) VALUES (4, 'ru-RU', 'Все включено');
INSERT INTO islandii.isl_meal_type_loc (mtp_id, mtl_locale, mtl_name) VALUES (1, 'ru-RU', 'Завтрак');
INSERT INTO islandii.isl_meal_type_loc (mtp_id, mtl_locale, mtl_name) VALUES (2, 'ru-RU', 'Завтрак и Ужин');
INSERT INTO islandii.isl_meal_type_loc (mtp_id, mtl_locale, mtl_name) VALUES (3, 'ru-RU', 'Завтрак, Обед и Ужин');

-- Meal
INSERT INTO islandii.isl_meal (mel_id, mel_crt_by, mel_crt_DATE, mel_upd_by, mel_upd_DATE, htl_id, mlt_id) VALUES (1, 1, '2020-07-18 18:24:52', 1, '2020-07-18 18:25:00', 1, 1);
INSERT INTO islandii.isl_meal (mel_id, mel_crt_by, mel_crt_DATE, mel_upd_by, mel_upd_DATE, htl_id, mlt_id) VALUES (2, 1, '2020-07-18 18:25:34', 2, '2020-07-18 18:25:39', 1, 2);
INSERT INTO islandii.isl_meal (mel_id, mel_crt_by, mel_crt_DATE, mel_upd_by, mel_upd_DATE, htl_id, mlt_id) VALUES (3, 1, '2020-07-18 18:25:36', 2, '2020-07-18 18:26:08', 2, 1);
INSERT INTO islandii.isl_meal (mel_id, mel_crt_by, mel_crt_DATE, mel_upd_by, mel_upd_DATE, htl_id, mlt_id) VALUES (4, 2, '2020-07-18 18:26:37', 1, '2020-07-18 18:26:40', 3, 1);
INSERT INTO islandii.isl_meal (mel_id, mel_crt_by, mel_crt_DATE, mel_upd_by, mel_upd_DATE, htl_id, mlt_id) VALUES (5, 2, '2020-07-18 18:26:38', 1, '2020-07-18 18:26:41', 3, 2);
INSERT INTO islandii.isl_meal (mel_id, mel_crt_by, mel_crt_DATE, mel_upd_by, mel_upd_DATE, htl_id, mlt_id) VALUES (6, 2, '2020-07-18 18:26:39', 1, '2020-07-18 18:26:42', 3, 3);
INSERT INTO islandii.isl_meal (mel_id, mel_crt_by, mel_crt_DATE, mel_upd_by, mel_upd_DATE, htl_id, mlt_id) VALUES (7, 2, '2020-07-18 18:26:39', 2, '2020-07-18 18:26:43', 3, 4);

-- Meal Price
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (1, 1, '2020-07-18 18:28:24', 1, '2020-07-18 18:28:27', 'USD', '2020-07-19', 0, 100.00, 1);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (2, 1, '2020-07-18 18:28:30', 1, '2020-07-18 18:28:28', 'USD', '2020-07-20', 0, 100.00, 1);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (3, 1, '2020-07-18 18:28:31', 1, '2020-07-18 18:28:29', 'USD', '2020-07-21', 0, 120.00, 1);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (4, 1, '2020-07-18 18:29:46', 1, '2020-07-18 18:29:49', 'USD', '2020-07-19', 1, 50.00, 1);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (5, 1, '2020-07-18 18:29:47', 2, '2020-07-18 18:29:50', 'USD', '2020-07-20', 1, 50.00, 1);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (6, 1, '2020-07-18 18:29:48', 2, '2020-07-18 18:29:50', 'USD', '2020-07-21', 1, 50.00, 1);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (7, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 0, 150.00, 2);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (8, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 0, 150.00, 2);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (9, 1, '2020-07-18 18:36:05', 1, '2020-07-18 18:36:00', 'USD', '2020-07-21', 1, 80.00, 2);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (10, 2, '2020-07-18 18:37:31', 2, '2020-07-18 18:37:33', 'USD', '2020-07-19', 0, 80.00, 3);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (11, 2, '2020-07-18 18:37:36', 2, '2020-07-18 18:37:37', 'USD', '2020-07-22', 0, 80.00, 3);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (12, 1, '2020-07-18 18:38:21', 1, '2020-07-18 18:38:17', 'USD', '2020-07-22', 1, 40.00, 3);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (13, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 0, 200.00, 4);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (14, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 0, 200.00, 4);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (15, 1, '2020-07-18 18:36:05', 1, '2020-07-18 18:36:00', 'USD', '2020-07-21', 0, 200.00, 4);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (16, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 1, 170.00, 4);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (17, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 1, 170.00, 4);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (18, 1, '2020-07-18 18:36:05', 1, '2020-07-18 18:36:00', 'USD', '2020-07-21', 1, 170.00, 4);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (19, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 0, 250.00, 5);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (20, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 0, 250.00, 5);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (21, 1, '2020-07-18 18:36:05', 1, '2020-07-18 18:36:00', 'USD', '2020-07-21', 0, 250.00, 5);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (22, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 1, 200.00, 5);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (23, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 1, 200.00, 5);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (24, 1, '2020-07-18 18:36:05', 1, '2020-07-18 18:36:00', 'USD', '2020-07-21', 1, 200.00, 5);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (25, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 0, 300.00, 6);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (26, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 0, 300.00, 6);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (27, 1, '2020-07-18 18:36:05', 1, '2020-07-18 18:36:00', 'USD', '2020-07-21', 0, 300.00, 6);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (28, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 1, 270.00, 6);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (29, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 1, 270.00, 6);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (30, 1, '2020-07-18 18:36:05', 1, '2020-07-18 18:36:00', 'USD', '2020-07-21', 1, 270.00, 6);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (31, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 0, 350.00, 7);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (32, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 0, 350.00, 7);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (33, 1, '2020-07-18 18:36:05', 1, '2020-07-18 18:36:00', 'USD', '2020-07-21', 0, 380.00, 7);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (34, 1, '2020-07-18 18:36:04', 1, '2020-07-18 18:35:58', 'USD', '2020-07-19', 1, 300.00, 7);
INSERT INTO islandii.isl_meal_price (mlp_id, mlp_crt_by, mlp_crt_DATE, mlp_upd_by, mlp_upd_DATE, mlp_currency, mlp_DATE, mlp_preferential, mlp_value, mel_id) VALUES (35, 1, '2020-07-18 18:36:08', 2, '2020-07-18 18:35:59', 'USD', '2020-07-20', 1, 340.00, 7);

-- To Do
INSERT INTO islandii.isl_todo (tdo_id, tdo_crt_by, tdo_crt_DATE, tdo_upd_by, tdo_upd_DATE) VALUES (1, 1, '2020-07-18 19:15:00', 1, '2020-07-18 19:15:02');
INSERT INTO islandii.isl_todo (tdo_id, tdo_crt_by, tdo_crt_DATE, tdo_upd_by, tdo_upd_DATE) VALUES (2, 2, '2020-07-18 19:15:09', 2, '2020-07-18 19:15:11');

-- To Do Locale
INSERT INTO islandii.isl_todo_loc (tdo_id, tdl_locale, tdl_name, tdl_text) VALUES (1, 'en-US', 'Diving', 'Diving is cool');
INSERT INTO islandii.isl_todo_loc (tdo_id, tdl_locale, tdl_name, tdl_text) VALUES (1, 'ru-RU', 'Дайвинг', 'Дайвинг это круто');
INSERT INTO islandii.isl_todo_loc (tdo_id, tdl_locale, tdl_name, tdl_text) VALUES (2, 'en-US', 'Snorkeling', 'Snarling is cool');
INSERT INTO islandii.isl_todo_loc (tdo_id, tdl_locale, tdl_name, tdl_text) VALUES (2, 'ru-RU', 'Сноркелинг', 'Сноркелинг это круто');

-- Transfer Type
INSERT INTO islandii.isl_transfer_type (trt_id, trt_crt_by, trt_crt_DATE, trt_upd_by, trt_upd_DATE) VALUES (1, 1, '2020-07-18 19:23:58', 2, '2020-07-18 19:23:59');
INSERT INTO islandii.isl_transfer_type (trt_id, trt_crt_by, trt_crt_DATE, trt_upd_by, trt_upd_DATE) VALUES (2, 1, '2020-07-18 19:24:03', 1, '2020-07-18 19:24:06');
INSERT INTO islandii.isl_transfer_type (trt_id, trt_crt_by, trt_crt_DATE, trt_upd_by, trt_upd_DATE) VALUES (3, 1, '2020-07-18 19:24:08', 1, '2020-07-18 19:24:01');

-- Transfer Type Locale
INSERT INTO islandii.isl_transfer_type_loc (ttp_id, ttl_locale, ttl_name) VALUES (3, 'en-US', 'Domestic flight + speedboat');
INSERT INTO islandii.isl_transfer_type_loc (ttp_id, ttl_locale, ttl_name) VALUES (2, 'en-US', 'Seaplane');
INSERT INTO islandii.isl_transfer_type_loc (ttp_id, ttl_locale, ttl_name) VALUES (1, 'en-US', 'Speedboat');
INSERT INTO islandii.isl_transfer_type_loc (ttp_id, ttl_locale, ttl_name) VALUES (3, 'ru-RU', 'Внутренний рейс + скоростная лодка');
INSERT INTO islandii.isl_transfer_type_loc (ttp_id, ttl_locale, ttl_name) VALUES (2, 'ru-RU', 'Самолет');
INSERT INTO islandii.isl_transfer_type_loc (ttp_id, ttl_locale, ttl_name) VALUES (1, 'ru-RU', 'Скоростная лодка');

-- Transfer
INSERT INTO islandii.isl_transfer (trf_id, trf_crt_by, trf_crt_DATE, trf_upd_by, trf_upd_DATE, htl_id, trt_id) VALUES (1, 1, '2020-07-18 19:28:43', 2, '2020-07-18 19:28:46', 1, 2);
INSERT INTO islandii.isl_transfer (trf_id, trf_crt_by, trf_crt_DATE, trf_upd_by, trf_upd_DATE, htl_id, trt_id) VALUES (2, 2, '2020-07-18 19:29:25', 1, '2020-07-18 19:29:22', 2, 1);
INSERT INTO islandii.isl_transfer (trf_id, trf_crt_by, trf_crt_DATE, trf_upd_by, trf_upd_DATE, htl_id, trt_id) VALUES (3, 1, '2020-07-18 19:29:34', 2, '2020-07-18 19:29:37', 3, 1);
INSERT INTO islandii.isl_transfer (trf_id, trf_crt_by, trf_crt_DATE, trf_upd_by, trf_upd_DATE, htl_id, trt_id) VALUES (4, 1, '2020-07-18 19:29:34', 2, '2020-07-18 19:29:37', 3, 2);
INSERT INTO islandii.isl_transfer (trf_id, trf_crt_by, trf_crt_DATE, trf_upd_by, trf_upd_DATE, htl_id, trt_id) VALUES (5, 1, '2020-07-18 19:29:34', 2, '2020-07-18 19:29:37', 3, 3);

-- Transfer Price
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (1, 1, '2020-07-18 21:36:47', 1, '2020-07-18 21:36:34', 'USD', '2020-07-19', 10, 100.00, 1);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (2, 1, '2020-07-18 21:36:48', 1, '2020-07-18 21:36:36', 'USD', '2020-07-20', 20, 100.00, 1);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (3, 1, '2020-07-18 21:36:49', 1, '2020-07-18 21:36:37', 'USD', '2020-07-19', 30, 200.00, 2);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (4, 1, '2020-07-18 21:36:50', 1, '2020-07-18 21:36:38', 'USD', '2020-07-20', 40, 200.00, 2);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (5, 1, '2020-07-18 21:36:50', 1, '2020-07-18 21:36:39', 'USD', '2020-07-19', 10, 300.00, 3);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (6, 1, '2020-07-18 21:36:51', 1, '2020-07-18 21:36:40', 'USD', '2020-07-20', 20, 300.00, 3);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (7, 1, '2020-07-18 21:36:52', 1, '2020-07-18 21:36:41', 'USD', '2020-07-19', 30, 330.00, 4);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (8, 1, '2020-07-18 21:36:52', 1, '2020-07-18 21:36:42', 'USD', '2020-07-20', 50, 300.00, 4);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (9, 1, '2020-07-18 21:36:53', 1, '2020-07-18 21:36:43', 'USD', '2020-07-19', 10, 200.00, 5);
INSERT INTO islandii.isl_transfer_price (trp_id, trp_crt_by, trp_crt_DATE, trp_upd_by, trp_upd_DATE, trp_currency, trp_DATE, trp_child_value, trp_value, trf_id) VALUES (10, 1, '2020-07-18 21:36:54', 1, '2020-07-18 21:36:44', 'USD', '2020-07-20', 10, 300.00, 5);

-- Villa Type
INSERT INTO islandii.isl_villa_type (vlt_id, vlt_crt_by, vlt_crt_DATE, vlt_upd_by, vlt_upd_DATE) VALUES (1, 1, '2020-07-18 19:49:20', 1, '2020-07-18 19:49:26');
INSERT INTO islandii.isl_villa_type (vlt_id, vlt_crt_by, vlt_crt_DATE, vlt_upd_by, vlt_upd_DATE) VALUES (2, 1, '2020-07-18 19:49:22', 1, '2020-07-18 19:49:28');
INSERT INTO islandii.isl_villa_type (vlt_id, vlt_crt_by, vlt_crt_DATE, vlt_upd_by, vlt_upd_DATE) VALUES (3, 1, '2020-07-18 19:50:19', 1, '2020-07-18 19:50:22');
INSERT INTO islandii.isl_villa_type (vlt_id, vlt_crt_by, vlt_crt_DATE, vlt_upd_by, vlt_upd_DATE) VALUES (4, 1, '2020-07-18 19:50:24', 1, '2020-07-18 19:50:26');
INSERT INTO islandii.isl_villa_type (vlt_id, vlt_crt_by, vlt_crt_DATE, vlt_upd_by, vlt_upd_DATE) VALUES (5, 1, '2020-07-18 19:50:23', 1, '2020-07-18 19:50:21');

-- Villa Type Locale
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (1, 'en-US', 'Beach View');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (2, 'en-US', 'Garden View');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (4, 'en-US', 'Villa with Pool');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (3, 'en-US', 'Water Villa');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (5, 'en-US', 'Water Villa with Pool');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (4, 'ru-RU', 'С бассейном');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (1, 'ru-RU', 'С видом на пляж');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (5, 'ru-RU', 'С видом на пляж и бассейном');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (2, 'ru-RU', 'С видом на сад');
INSERT INTO islandii.isl_villa_type_loc (vlt_id, vtl_locale, vtl_name) VALUES (3, 'ru-RU', 'У воды');

-- Villa
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (1, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 1, 1, 1, 0, 1, 1, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (2, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 2, 1, 1, 0, 1, 1, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (3, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 3, 1, 1, 0, 1, 1, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (4, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 2, 1, 1, 0, 1, 2, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (5, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 1, 1, 1, 0, 1, 3, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (6, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 1, 1, 1, 0, 2, 1, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (7, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 1, 1, 1, 0, 2, 2, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (8, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 1, 1, 1, 0, 3, 1, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (9, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 2, 1, 1, 0, 3, 1, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (10, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 3, 1, 1, 0, 3, 1, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (11, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 1, 1, 1, 0, 3, 2, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (12, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 1, 1, 1, 0, 3, 3, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (13, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 1, 1, 1, 0, 3, 4, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (14, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 2, 1, 1, 0, 3, 5, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (15, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 2, 1, 1, 0, 3, 2, 3, 1);
INSERT INTO islandii.isl_villa (vil_id, vil_crt_by, vil_crt_DATE, vil_upd_by, vil_upd_DATE, vil_images, vil_adult_place_quantity, vil_occasional_adult_place_quantity, vil_child_place_quantity, vil_occasional_child_place_quantity, htl_id, vlt_id, vil_number, vil_meal_type) VALUES (16, 1, '2020-07-18 19:55:30', 1, '2020-07-18 19:55:32', '', 3, 1, 1, 0, 3, 3, 3, 1);

-- Villa Locale
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (1, 'en-US', 'Single with beach view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (1, 'ru-RU', 'Одиночная с видом на пляж');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (2, 'en-US', 'Double with beach view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (2, 'ru-RU', 'Двойная с видом на пляж');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (3, 'en-US', 'Triple with beach view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (3, 'ru-RU', 'Тройная с видом на пляж');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (4, 'en-US', 'Double with garden view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (4, 'ru-RU', 'Двойная с видом на сад');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (5, 'en-US', 'Single water Villa');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (5, 'ru-RU', 'Одиночная у воды');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (6, 'en-US', 'Single with beach view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (6, 'ru-RU', 'Одиночная с видом на пляж');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (7, 'en-US', 'Single with garden view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (7, 'ru-RU', 'Одиночная с видом на сад');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (8, 'en-US', 'Single with beach view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (8, 'ru-RU', 'Одиночная с видом на пляж');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (9, 'en-US', 'Double with beach view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (9, 'ru-RU', 'Двойная с видом на пляж');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (10, 'en-US', 'Triple with beach view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (10, 'ru-RU', 'Тройная с видом на пляж');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (11, 'en-US', 'Single with garden view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (11, 'ru-RU', 'Одиночная с видом на сад');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (12, 'en-US', 'Single water Villa');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (12, 'ru-RU', 'Одиночная у воды');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (13, 'en-US', 'Single villa with Pool');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (13, 'ru-RU', 'Одиночная с бассейном');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (14, 'en-US', 'Double water Villa with Pool');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (14, 'ru-RU', 'Двойная у воды с байссейном');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (15, 'en-US', 'Double with garden view');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (15, 'ru-RU', 'Двойная с видом на сад');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (16, 'en-US', 'Triple water Villa');
INSERT INTO islandii.isl_villa_loc (vil_id, vll_locale, vll_name) VALUES (16, 'ru-RU', 'Тройная у воды');

-- Villa price
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (1, 2, '2020-07-18 21:39:47', 1, '2020-07-18 21:40:01', 'USD', '2020-07-19', 100.00, 1);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (2, 2, '2020-07-18 21:39:49', 1, '2020-07-18 21:40:02', 'USD', '2020-07-19', 110.00, 2);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (3, 2, '2020-07-18 21:39:49', 1, '2020-07-18 21:40:02', 'USD', '2020-07-19', 130.00, 3);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (4, 2, '2020-07-18 21:39:50', 1, '2020-07-18 21:40:03', 'USD', '2020-07-19', 110.00, 4);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (5, 2, '2020-07-18 21:39:51', 1, '2020-07-18 21:40:04', 'USD', '2020-07-19', 130.00, 5);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (6, 2, '2020-07-18 21:39:51', 1, '2020-07-18 21:40:07', 'USD', '2020-07-19', 150.00, 6);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (7, 2, '2020-07-18 21:39:52', 1, '2020-07-18 21:40:07', 'USD', '2020-07-19', 110.00, 7);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (8, 2, '2020-07-18 21:39:53', 1, '2020-07-18 21:40:08', 'USD', '2020-07-19', 100.00, 8);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (9, 2, '2020-07-18 21:39:53', 1, '2020-07-18 21:40:09', 'USD', '2020-07-19', 170.00, 9);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (10, 2, '2020-07-18 21:39:55', 1, '2020-07-18 21:40:09', 'USD', '2020-07-19', 140.00, 10);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (11, 2, '2020-07-18 21:39:54', 1, '2020-07-18 21:40:10', 'USD', '2020-07-19', 100.00, 11);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (12, 2, '2020-07-18 21:39:57', 1, '2020-07-18 21:40:11', 'USD', '2020-07-19', 180.00, 12);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (13, 2, '2020-07-18 21:39:58', 1, '2020-07-18 21:40:11', 'USD', '2020-07-19', 200.00, 13);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (14, 2, '2020-07-18 21:39:58', 1, '2020-07-18 21:40:12', 'USD', '2020-07-19', 260.00, 14);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (15, 2, '2020-07-18 21:39:59', 1, '2020-07-18 21:40:13', 'USD', '2020-07-19', 230.00, 15);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (16, 2, '2020-07-18 21:40:00', 1, '2020-07-18 21:40:13', 'USD', '2020-07-19', 190.00, 16);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (17, 2, '2020-07-18 21:39:47', 1, '2020-07-18 21:40:01', 'USD', '2020-07-20', 100.00, 1);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (18, 2, '2020-07-18 21:39:49', 1, '2020-07-18 21:40:02', 'USD', '2020-07-20', 170.00, 2);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (19, 2, '2020-07-18 21:39:49', 1, '2020-07-18 21:40:02', 'USD', '2020-07-20', 140.00, 3);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (20, 2, '2020-07-18 21:39:50', 1, '2020-07-18 21:40:03', 'USD', '2020-07-20', 100.00, 4);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (21, 2, '2020-07-18 21:39:51', 1, '2020-07-18 21:40:04', 'USD', '2020-07-20', 200.00, 5);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (22, 2, '2020-07-18 21:39:51', 1, '2020-07-18 21:40:07', 'USD', '2020-07-20', 100.00, 6);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (23, 2, '2020-07-18 21:39:52', 1, '2020-07-18 21:40:07', 'USD', '2020-07-20', 170.00, 7);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (24, 2, '2020-07-18 21:39:53', 1, '2020-07-18 21:40:08', 'USD', '2020-07-20', 140.00, 8);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (25, 2, '2020-07-18 21:39:53', 1, '2020-07-18 21:40:09', 'USD', '2020-07-20', 100.00, 9);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (26, 2, '2020-07-18 21:39:55', 1, '2020-07-18 21:40:09', 'USD', '2020-07-20', 100.00, 10);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (27, 2, '2020-07-18 21:39:54', 1, '2020-07-18 21:40:10', 'USD', '2020-07-20', 180.00, 11);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (28, 2, '2020-07-18 21:39:57', 1, '2020-07-18 21:40:11', 'USD', '2020-07-20', 200.00, 12);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (29, 2, '2020-07-18 21:39:58', 1, '2020-07-18 21:40:11', 'USD', '2020-07-20', 260.00, 13);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (30, 2, '2020-07-18 21:39:58', 1, '2020-07-18 21:40:12', 'USD', '2020-07-20', 230.00, 14);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (31, 2, '2020-07-18 21:39:59', 1, '2020-07-18 21:40:13', 'USD', '2020-07-20', 190.00, 15);
INSERT INTO islandii.isl_villa_price (vlp_id, vlp_crt_by, vlp_crt_DATE, vlp_upd_by, vlp_upd_DATE, vlp_currency, vlp_DATE, vlp_value, vil_id) VALUES (32, 2, '2020-07-18 21:40:00', 1, '2020-07-18 21:40:13', 'USD', '2020-07-20', 100.00, 16);